<?php

use Illuminate\Database\Seeder;
use App\Message;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(MessagesSeeder::class);
    }
}
