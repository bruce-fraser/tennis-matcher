<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 2000)->create()->each(function ($u) {
            $ava = factory(App\Image::class)->make();
            $ava->user()->associate($u);
            $ava->save();
            $u->avatar_id = $ava->id; $u->save();
            $rn = rand(1, 5);
            for ($i = 0; $i < $rn; $i++) {
                $u->playLocations()->save(factory(App\PlayLocation::class)->make());
            }
        });
    }
}
