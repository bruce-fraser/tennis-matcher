<?php

use Illuminate\Database\Seeder;

class MessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numUsers = 600;
        $randCollection = App\User::all()->random($numUsers);
        for ($i=0; $i<$numUsers-1; $i++) {
            $mess = factory(App\Message::class)->make();
            $mess->user()->associate($randCollection->values()[$i]);
            $mess->save();
            $mess->touchLastReply();
            $mess->users()->sync([$randCollection->values()[$i]->id, $randCollection->values()[$i+1]->id]);
            for ($n=0; $n<40; $n++) {
              $reply = factory(App\Message::class)->make();
              $reply->user()->associate($randCollection->values()[$i+1]);
              $mess->replies()->save($reply);
              $mess->touchLastReply();
              $reply = factory(App\Message::class)->make();
              $reply->user()->associate($randCollection->values()[$i]);
              $mess->replies()->save($reply);
              $mess->touchLastReply();
            }
        }
    }
}
