<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'uid' => uniqid(true),
        'gender' => $faker->randomElement(['male', 'female']),
        'skill_level' => $faker->randomElement(['3.0', '3.5', '4.0', '4.5', '5.0']),
        'active' => 1,
        'right_left' => $faker->randomElement([1, 2]),
        'birthdate' => date('Y-m-d', mt_rand(-473364000, 923205600)),
        'intro' => $faker->paragraph
    ];
});

$factory->define(App\Image::class, function (Faker\Generator $faker) {

    return [
        'path' => $faker->randomElement(['/158e2c656b7070.png', '/158e2c6e460827.png', '/158e2c5ef848ff.png', '/158f71dd577770.png', '/158e2c7bc97636.png', '/1595f03b299d77.png'])
    ];
});

$factory->define(App\PlayLocation::class, function (Faker\Generator $faker) {

    return [
        'latitude' => $faker->latitude($min = -90, $max = 90),
        'longitude' => $faker->longitude($min = -180, $max = 180),
        'city' => $faker->city,
        'available' => $faker->randomElement([0, 1]),
        'singles_doubles' => $faker->randomElement([0, 1, 2]),
        'need_partner' => $faker->randomElement([1, 2]),
        'play_with' => $faker->randomElement([1, 2, 3]),
    ];
});

$factory->define(App\Message::class, function (Faker\Generator $faker) {

    return [
        'body' => nl2br(e($faker->paragraph)),
    ];
});
