<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function () {
    if (auth()->check()) {
      auth()->user()->touchUser();
    }
    $user = App\User::where('email', config('app.admins')[0])->first();
    return view('welcome', compact('user'));
})->name('welcome');

Route::get('/terms', function () {
    return view('terms');
})->name('terms');

Route::get('/privacy', function () {
    return view('privacy');
})->name('privacy');

Route::get('/contact-us', function () {
    return view('contact');
})->name('contact');

Route::get('/tennis-links', function () {
    return view('links');
})->name('links');

Auth::routes();

Route::get('user/invoice/{invoice}', function (Request $request, $invoiceId) {
    return $request->user()->downloadInvoice($invoiceId, [
      'vendor' => 'Bruce Fraser / ' . config('app.name'),
      'product' => 'monthly subscription'
    ]);
});

Route::post('/stripe/webhook', 'WebhookController@handleWebhook')->name('stripe.webhook');

Route::get('/user/account', 'UserAccountController@show')->name('user.account.show');
Route::post('/user/account', 'UserAccountController@updatePassword')->name('user.account.password');

Route::post('/subscription', 'SubscriptionController@store')->name('subscription.store');
Route::post('/subscription/resume', 'SubscriptionController@resume')->name('subscription.resume');
Route::delete('/subscription', 'SubscriptionController@destroy')->name('subscription.delete');
Route::post('/subscription/updatecard', 'SubscriptionController@updateCard')->name('subscription.updateCard');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@showProfile')->name('profile.show');
Route::get('/locations', 'HomeController@indexLocations')->name('locations.index');
Route::post('/locations', 'HomeController@storeLocation')->name('locations.store');
Route::delete('/locations/{playLocation}', 'HomeController@deleteLocation')->name('locations.delete');
Route::put('/locations/{playLocation}', 'HomeController@updateLocation')->name('locations.update');
Route::put('/profile', 'HomeController@updateProfile')->name('profile.update');

Route::get('/activate/token/{token}', 'Auth\ActivationController@activate')->name('auth.activate');
Route::get('/activate/resend', 'Auth\ActivationController@resend')->name('auth.activate.resend');
Route::get('/tennis-partners', 'SearchController@index')->name('search.index');
Route::get('/tennis-partner/{user}', 'SearchController@show')->name('search.show');
Route::get('/search/{user}', 'SearchController@show');
Route::post('/avatar', 'AvatarController@store')->name('avatar.store');
Route::get('/avatar/delete', 'AvatarController@delete');

Route::post('/players/{user}/favorites', 'PlayerFavoriteController@store')->name('players.favorites.store');
Route::get('/players/favorites', 'PlayerFavoriteController@index')->name('players.favorites.index');
Route::delete('/players/{user}/favorites', 'PlayerFavoriteController@destroy')->name('players.favorites.destroy');

Route::post('/players/{user}/block', 'PlayerBlockController@store')->name('players.block.store');
Route::delete('/players/{user}/unblock', 'PlayerBlockController@destroy')->name('players.block.destroy');

Route::get('/players/{user}/votes', 'PlayerVoteController@show')->name('players.votes.show');
Route::post('/players/{user}/votes', 'PlayerVoteController@store')->name('players.votes.store');
Route::delete('/players/{user}/votes', 'PlayerVoteController@destroy')->name('players.votes.destroy');

Route::put('/players/{user}/reviews', 'PlayerReviewController@update')->name('players.reviews.update');
Route::post('/players/{user}/reviews', 'PlayerReviewController@store')->name('players.reviews.store');
Route::delete('/players/{user}/reviews', 'PlayerReviewController@destroy')->name('players.reviews.destroy');

Route::get('/inbox', 'InboxController@index')->name('inbox.index');
Route::get('/inbox/{message}', 'InboxController@show');

Route::group(['prefix' => 'webapi', 'namespace' => 'Api'], function () {
    Route::get('/inbox', 'InboxController@index');
    Route::post('/inbox', 'InboxController@store')->name('inbox.store');
    Route::get('/inbox/{message}', 'InboxController@show');
    Route::get('/inbox/{message}/read', 'InboxController@updatePivot');
    Route::post('/inbox/{message}/reply', 'ReplyController@store');
});

Route::get('/admin', 'AdminController@index')->name('admin.index');
Route::get('/admin/settings', 'AdminController@showSettings')->name('admin.settings');
Route::post('/admin/settings', 'AdminController@updateSettings')->name('admin.updateSettings');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('admin')->name('admin.logs');
