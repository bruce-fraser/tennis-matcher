<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Vote extends Model
{
    protected $fillable = ['type', 'user_id'];

    public function voteable()
    {
      return $this->morphTo();
    }

    public function user()
    {
      return $this->belongsTo(User::class);
    }

}
