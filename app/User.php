<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'gender', 'skill_level', 'birthdate', 'active', 'avatar_id', 'last_email_notification', 'right_left', 'intro', 'uid'];
    protected $hidden = ['password', 'remember_token', 'pivot', 'avatar'];
    protected $appends = ['avatar_path', 'is_subscriber'];
    protected $dates = ['last_email_notification', 'birthdate'];

/*
 *  R E L A T I O N S H I P S
 */
     public function playLocations()
     {
       return $this->hasMany(PlayLocation::class);
     }

     public function activationToken()
     {
         return $this->hasOne(ActivationToken::class);
     }

     public function avatar()
     {
         return $this->hasOne(Image::class, 'id', 'avatar_id');
     }

     public function messages()
     {
        //  return $this->belongsToMany(Message::class)->whereNull('parent_id')->whereNotIn('pivot.user_id', $this->blockedUsers->pluck('id')->toArray())->withPivot('unread')->orderBy('last_reply', 'desc');
         return $this->belongsToMany(Message::class)->whereNull('parent_id')->withPivot('unread')->orderBy('last_reply', 'desc');
     }

     public function favoritePlayers()
     {
         $ids  = $this->blockedUsers->pluck('id')->toArray();
         $ids2 = $this->blockedByUsers->pluck('id')->toArray();
         $arr = array_merge($ids, $ids2, [$this->id]);

         return $this->morphedByMany(User::class, 'favoritable')->whereNotIn('favoritable_id', $arr);
     }

     public function favoritedByPlayers()
     {
       return $this->morphToMany(User::class, 'favoritable');
     }

     public function blockedUsers()
     {
       return $this->belongsToMany(User::class, 'blocker_blocked', 'blocker_id', 'blocked_id');
     }

     public function blockedByUsers()
     {
       return $this->belongsToMany(User::class, 'blocker_blocked', 'blocked_id', 'blocker_id');
     }
/*
 *  E N D  R E L A T I O N S H I P S
 */

    public static function staticAvatar($uEmail, $size = 45)
    {
        return 'https://www.gravatar.com/avatar/' . md5($uEmail) . '?s=' . $size . '&d=mm';
    }


    public static function byEmail($email)
    {
        return static::where('email', $email);
    }

/*
 *  S C O P E S
 */
    public function scopeForGender($query, $gender = 'any')
    {
        if ($gender == 'any') { return $query; }

        return $query->where('gender', $gender);
    }

    public function scopeMinutesOffsite($query, $minutes = 10)
    {
        return $query->where('updated_at', '<', Carbon::now()->subMinutes($minutes));
    }

    public function scopeHasUnread($query)
    {
        return $query->whereHas('messages', function($q) {
            $q->where('unread', true);
        });
    }

    public function scopeOnlineSinceLast($query)
    {
        return $query->whereRaw('last_email_notification < updated_at');
    }
/*
 *  E N D  S C O P E S
 */

/*
 *  V O T A B L E
 */
     public function votes()
     {
         return $this->morphMany(Vote::class, 'voteable');
     }

     public function upVotes()
     {
         return $this->votes->where('type', 'up');
     }

     public function downVotes()
     {
         return $this->votes->where('type', 'down');
     }

     public function voteFromUser(User $user)
     {
         return $this->votes->where('user_id', $user->id);
     }
/*
 *  E N D  V O T A B L E
 */

/*
 *  R E V I E W A B L E
 */
     public function reviews()
     {
         return $this->morphMany(Review::class, 'reviewable');
     }

     public function reviewedByUser(User $user)
     {
         return (bool) $this->reviews->where('user_id', $user->id)->count();
     }
/*
 *  E N D  R E V I W A B L E
 */

    public function avatarPath($size=100)
    {
        if (!$this->avatar_id) {
            return 'https://www.gravatar.com/avatar/' . md5($this->email) . '?s=' . $size . '&d=mm';
        }

        return $this->avatar->path();
    }

    public function touchUser()
    {
        $this->updated_at = \Carbon\Carbon::now();
        $this->save();
    }

    public function isInConversation(Message $message)
    {
        return $this->messages->contains($message);
    }

    public function getAvatarPathAttribute()
    {
        return $this->avatarPath(100);  // changed from 45
    }

    public function favoritedBy(User $user)
    {
      return $this->favoritedByPlayers->contains($user);
    }

    public function votesAllowed()
    {
      return true;
      //return (bool) $this->allow_votes;
    }

    public function getRouteKeyName()
    {
        return 'uid';
    }

    public function isAdmin()
    {
        return in_array($this->email, config('app.admins', ['bfras33@yahoo.com']));
    }

    public function isSubscriber()
    {
        return $this->subscribed('standard') || $this->isAdmin();
    }

    public function getIsSubscriberAttribute()
    {
        return (bool) $this->isSubscriber();
    }
}
