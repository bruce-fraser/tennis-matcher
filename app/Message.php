<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Message extends Model
{
    protected $hidden = ['created_at'];
    protected $dates = ['last_reply'];
    protected $appends = ['created_at_human', 'last_reply_human', 'latest_reply'];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function users()
    {
      return $this->belongsToMany(User::class);
    }

    public function usersExceptCurrentlyAuthenticated()
    {
        return $this->users()->where('user_id', '!=', \Auth::user()->id);
    }

    public function replies()
    {
        return $this->hasMany(Message::class, 'parent_id')->latest();
    }

    public function parent()
    {
        return $this->belongsTo(Message::class, 'parent_id');
    }

    /*
     *  S C O P E S
     */
     public function scopeNotBlocked($query, User $user)
     {
          $ids  = $user->blockedUsers->pluck('id')->toArray();
          $ids2 = $user->blockedByUsers->pluck('id')->toArray();
          $arr = array_merge($ids, $ids2, [$user->id]);

         return $query->whereHas('users', function($q) use ($arr) {
             $q->whereNotIn('user_id', $arr);
         });
     }
     /*
      *  E N D  S C O P E S
      */


    public function touchLastReply()
    {
        $this->last_reply = \Carbon\Carbon::now();
        $this->save();
    }

    public function isReply()
    {
        return $this->parent_id !== null;
    }

    public function getCreatedAtHumanAttribute()
    {
      return $this->created_at->diffForHumans();
    }

    public function getLastReplyHumanAttribute()
    {
      return $this->last_reply ? $this->last_reply->diffForHumans() : null;
    }

    public function getLatestReplyAttribute()
    {
      return $this->replies()->latest()->pluck('body')->first();
    }
}
