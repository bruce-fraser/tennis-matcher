<?php

namespace App\Policies;

use App\Message;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;


    public function show(User $user, Message $message)
    {
        return $this->affect($user, $message);
    }

    public function affect(User $user, Message $message)
    {
        $otherUser = $message->usersExceptCurrentlyAuthenticated()->first();

        return $user->isInConversation($message) && ! ($user->blockedByUsers()->get()->contains($otherUser) || $user->blockedUsers()->get()->contains($otherUser));
    }
}
