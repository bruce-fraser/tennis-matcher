<?php

namespace App\Policies;

use App\User;
use App\PlayLocation;
use Illuminate\Auth\Access\HandlesAuthorization;

class LocationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function delete(User $user, PlayLocation $playLocation) {
        // return true;
        return $user->id === $playLocation->user_id;
    }  
}
