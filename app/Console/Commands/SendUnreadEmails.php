<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Mail\UnreadMessagesEmail;
use Mail;

class SendUnreadEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-unread-emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email to users with unread messages.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::onlineSinceLast()->hasUnread()->minutesOffsite()->get();
        if ($users->isNotEmpty()) {
            //mail users
            foreach($users as $user) {
              try {
                Mail::to($user)->send(new UnreadMessagesEmail());
              } catch (\Exception $e) {
                Log::info('Email to ' . $user->email . ' not successful.');
              }
            }
            //update last_email_notification
            $users->each(function ($user) {
              $user->last_email_notification = \Carbon\Carbon::now()->toDateTimeString();
              $user->timestamps = false;
              $user->save();
            });

            Log::info('Emailed: ' . $users->count() . ' users = '. $users->pluck('email'));
        } else {
            Log::info('No users emailed for unread messages.');
        }
    }
}
