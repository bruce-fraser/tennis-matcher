<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['body', 'user_id'];

    public function reviewable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*
     *  S C O P E S
     */
     public function scopeNotBlocked($query, User $user=null)
     {
          if (!$user) {
            return $query;
          }
          $ids  = $user->blockedUsers->pluck('id')->toArray();
          $ids2  = $user->blockedByUsers->pluck('id')->toArray();
          $arr = array_merge($ids, $ids2);

         return $query->whereNotIn('user_id', $arr);
     }
     /*
      *  E N D  S C O P E S
      */
}
