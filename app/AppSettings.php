<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class AppSettings extends Model
{
    public static function getFreePromoPeriod()
    {
      return Cache::get('free-promo-period', true);
    }

    public static function setFreePromoPeriod($value)
    {
      Cache::forever('free-promo-period', $value);
      return true;
    }

    public static function getTakingNewSubscriptions()
    {
      return Cache::get('taking-new-subscriptions', false);
    }

    public static function setTakingNewSubscriptions($value)
    {
      Cache::forever('taking-new-subscriptions', $value);
      return true;
    }

    public static function getSubscriptionPrice()
    {
      return Cache::get('subscription-price', 5.99);
    }

    public static function setSubscriptionPrice($value)
    {
      Cache::forever('subscription-price', $value);
      return true;
    }

    public static function getHideSite()
    {
      return Cache::get('hide-site', false);
    }

    public static function setHideSite($value)
    {
      Cache::forever('hide-site', $value);
      return true;
    }

    public static function getRequireRegisterSearch()
    {
      return Cache::get('require-register-search', true);
    }

    public static function setRequireRegisterSearch($value)
    {
      Cache::forever('require-register-search', $value);
      return true;
    }
}
