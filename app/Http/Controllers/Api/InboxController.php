<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\ConversationCreated;
use App\Events\ConversationReplyCreated;

class InboxController extends Controller
{
    public function __construct()
    {
      $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
      // $conversations = $request->user()->messages()->with(['user', 'users'])->get();
      $conversations = $request->user()->messages()->notBlocked($request->user())->with(['user', 'users'])->get();
      // $conversations = $request->user()->messages()->with(['user', 'users'])->whereRaw('NOT((messages.user_id = ?) AND (last_reply IS NULL))', $request->user()->id)->get();

      return $conversations;
    }

    public function show(Message $message)
    {

        $this->authorize('show', $message);

        if ($message->isReply()) {
            abort(404);
        }
        // return $message;
        return $message->load(['user', 'users', 'replies', 'replies.user']);
    }

    public function store(Request $request)
    {
        // dd($request);
        $this->validate($request, [
           'body' => 'required|max:2000',
           'recipient' => 'required|exists:users,id'
        ]);
        if (count($messagesUser = $request->user()->messages()->get())) {
            if (count($messagesRecipient = User::find($request->recipient)->messages()->get())) {
                $foundMessage = null;
                $messagesUser->each(function ($item) use ($messagesRecipient, &$foundMessage) {
                   if ($messagesRecipient->contains($item)) {
                      $foundMessage = $item;
                   }
                });
                if ($foundMessage) {
                    // dd($foundMessage);
                    $reply = new Message;
                    $reply->body = nl2br(e($request->body));
                    $reply->user()->associate($request->user());

                    $foundMessage->replies()->save($reply);
                    $foundMessage->touchLastReply();

                    broadcast(new ConversationReplyCreated($reply))->toOthers();

                    User::find($request->recipient)->messages()->updateExistingPivot($foundMessage->id, ['unread' => true]);

                    return back()->withSuccess('message sent');
                }
            }
        }
        $message = new Message;
        $message->body = nl2br(e($request->body));
        $message->user()->associate($request->user());
        $message->save();

        $message->touchLastReply();

        $message->users()->sync([$request->recipient, $request->user()->id]);
        broadcast(new ConversationCreated($message))->toOthers();

        User::find($request->recipient)->messages()->updateExistingPivot($message->id, ['unread' => true]);

        return back()->withSuccess('message sent');
    }

    public function updatePivot(Message $message)
    {
        $this->authorize('show', $message);

        if ($message->isReply()) {
            abort(404);
        }
        auth()->user()->messages()->updateExistingPivot($message->id, ['unread' => false]);
        // return $message;
        return 'hit updatePivot for message id ' . $message->id;
    }
}
