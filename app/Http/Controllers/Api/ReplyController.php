<?php

namespace App\Http\Controllers\Api;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\ConversationReplyCreated;

class ReplyController extends Controller
{
    public function __construct()
    {
      $this->middleware(['auth']);
    }

    public function store(Request $request, Message $message) {
        $this->authorize('show', $message);

        $this->validate($request, [
           'body' => 'required|max:2000',
        ]);

        $reply = new Message;
        $reply->body = nl2br(e($request->body));
        // $reply->body = $request->body;
        $reply->user()->associate($request->user());

        $message->replies()->save($reply);
        $message->touchLastReply();

        broadcast(new ConversationReplyCreated($reply))->toOthers();

        $message->usersExceptCurrentlyAuthenticated()->get()->each(function ($user) use ($message) {
          $user->messages()->updateExistingPivot($message->id, ['unread' => true]);
        });

        return $reply->load(['user', 'parent', 'parent.user', 'parent.users']);
      }
}
