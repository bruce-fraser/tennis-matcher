<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;
use App\Image;
use Intervention\Image\ImageManager;
use Illuminate\Validation\Rule;

class AvatarController extends Controller
{
    protected $imageManager;

    public function __construct(ImageManager $imageManager)
    {
      // die('here???');
      $this->middleware(['auth']);
      $this->imageManager = $imageManager;
    }
    public function store(Request $request)
    {
        $this->validate($request, [
          'image' => 'required|image|max:4000'
        ], [
          'image.image' => 'Hmm, not a valid image.'
        ]);

        $processedImage = $this->imageManager->make($request->file('image')->getPathName())
            ->orientate()
            ->fit(100, 100, function ($c) {
                $c->aspectRatio();
            })
            ->encode('png')
            ->save(config('image.path.absolute') . $path = '/' . uniqid(true) . '.png');

        $image = new Image;
        $image->path = $path;
        $image->user()->associate($request->user());
        $image->save();

        if ($request->user()->avatar) {
            $this->deleteHelper($request->user());
        }

        $request->user()->update(['avatar_id' => $image->id]);

        return response([
            'data' => [
                'id' => $image->id,
                'path' => $image->path(),
            ]
        ], 200);
    }

    public function delete(Request $request)
    {
        $user = $request->user();

        if ($this->deleteHelper($user)) {
            return response(null, 200);
        }
        return response(null, 400);
    }

    protected function deleteHelper($userObject)
    {
        $file = public_path() . '/' . $userObject->avatarPath();

        if ($userObject->avatar()->delete()) {
            File::delete($file);
            $userObject->avatar_id = null;
            $userObject->save();
            return true;
        }
        return false;
    }
}
