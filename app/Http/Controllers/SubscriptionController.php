<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function __construct()
    {
      $this->middleware(['auth']);
    }

    public function store(Request $request) {

        if ($request->user()->newSubscription('standard', 'monthly1')->create($request->stripeToken)) {
          return back()->withSuccess('thank you for your subscription!');
        } else {
          return back()->withErrors(['something went wrong. you may try again or try a different card, perhaps.']);
        }
    }

    public function destroy(Request $request) {
        $request->user()->subscription('standard')->cancel();
        return back()->withSuccess('your subscription has been cancelled.');
    }

    public function resume(Request $request) {
        $request->user()->subscription('standard')->resume();
        return back()->withSuccess('your subscription has been resumed.');
    }

    public function updateCard(Request $request) {
        $request->user()->updateCard($request->stripeToken);
        return back()->withSuccess('your card has been updated.');
    }
}
