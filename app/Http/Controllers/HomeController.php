<?php

namespace App\Http\Controllers;

use App\PlayLocation;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->touchUser();
        return view('home');
    }

    public function showProfile()
    {
        $registeredUser = Auth::user();

        return response()->json([
          'user_name' => $registeredUser->name,
          'skill_level' => $registeredUser->skill_level,
          'birth_date' => Carbon::parse($registeredUser->birthdate)->toFormattedDateString(),
          'gender' => $registeredUser->gender,
          'right_left' => $registeredUser->right_left == 1 ? 'right-handed': 'left-handed',
          'intro' => nl2br(e($registeredUser->intro))
        ]);
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [
          'name' => 'nullable|max:255',
          'skill' => 'nullable|in:2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,7.0',
          'birthdate' => 'nullable|date',
          'gender' => 'nullable|in:male,female',
          'right_left' => 'nullable|numeric',
          'intro' => 'nullable|max:1000'
        ]);

        $user = $request->user();
        $user->name = $request->name ?? $user->name;
        $user->skill_level = $request->skill ?? $user->skill_level;
        $user->birthdate = $request->birthdate ? date('Y-m-d', strtotime($request->birthdate)) : $user->birthdate;
        $user->gender = $request->gender ?? $user->gender;
        $user->right_left = $request->right_left ?? $user->right_left;
        $user->intro = $request->intro ?? $user->intro;
        $user->save();

        return response()->json(null, 200);
    }

    public function indexLocations(Request $request)
    {
        $registeredUser = Auth::user();

        // return $registeredUser->playLocations()->orderBy('created_at')->get();
        return $registeredUser->playLocations()->oldest()->get();
    }

    public function storeLocation(Request $request)
    {
        $registeredUser = Auth::user();

        $this->validate($request, [
          'city' => 'min:2|max:255',
          'lat' => 'numeric',
          'long' => 'numeric'
        ]);

        $registeredUser->playLocations()->create([
            'city' => $request->city,
            'latitude' => $request->lat,
            'longitude' => $request->long
        ]);


        return response()->json(null, 200);
    }

    public function deleteLocation(PlayLocation $playLocation) {
        $this->authorize('delete', $playLocation);
        $playLocation->delete();
        return response()->json(null, 200);
    }

    public function updateLocation(Request $request, PlayLocation $playLocation)
    {
        $this->authorize('delete', $playLocation);
        // Log::info('update location test');
        $this->validate($request, [
          'available' => 'nullable|boolean',
          'singles_doubles' => 'nullable|numeric',
          'need_partner' => 'nullable|numeric',
          'play_with' => 'nullable|numeric'
        ]);

        switch(true) {
            case $request->has('available'):
                $playLocation->available = $request->available;
                break;
            case $request->has('singles_doubles'):
                $playLocation->singles_doubles = $request->singles_doubles;
                break;
            case $request->has('need_partner'):
                $playLocation->need_partner = $request->need_partner;
                break;
            case $request->has('play_with'):
                $playLocation->play_with = $request->play_with;
                break;
            default:
                Log::error('update location error ');
         }

        $playLocation->save();

        return response()->json(null, 200);
    }
}
