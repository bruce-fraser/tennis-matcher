<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PlayerVoteController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth')->except('show');
    }

    public function show(Request $request, User $user)
    {
        $response = [
            'up' => null,
            'down' => null,
            'can_vote' => $user->votesAllowed() && (bool) $request->user(),
            'user_vote' => null,
        ];

        if ($user->votesAllowed()) {
            $response['up'] = $user->upVotes()->count();
            $response['down'] = $user->downVotes()->count();
        }

        if ($request->user()) {
            $voteFromUser = $user->voteFromUser($request->user())->first();
            $response['user_vote'] = $voteFromUser ? $voteFromUser->type : null;
        }

        return response()->json([
            'data' => $response
        ], 200);
    }

    public function store(Request $request, User $user)
    {
        // $this->authorize('vote', $user);

        $voteRecord = $user->voteFromUser($request->user())->first();
        if ($voteRecord) {
            $voteRecord->delete();
        }

        $user->votes()->create([
            'type' => $request->type,
            'user_id' => $request->user()->id,
        ]);

        return response()->json(null, 200);
    }

    public function destroy(Request $request, User $user)
    {
        // $this->authorize('vote', $user);

        $voteRecord = $user->voteFromUser($request->user())->first();
        if ($voteRecord) {
            $voteRecord->delete();
        }

        return response()->json(null, 200);
    }
}
