<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PlayerReviewController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function store(Request $request, User $user)
  {
      // $this->authorize('vote', $user);
      $this->validate($request, [
          'review_field' => 'required|max:2000'
      ]);

      $user->reviews()->create([
          'body' => nl2br(e($request->review_field)),
          'user_id' => $request->user()->id,
      ]);

      return back()->withSuccess('review added successfully!');
  }

  public function destroy(Request $request, User $user)
  {
    $review = $user->reviews->where('user_id', $request->user()->id)->first();

    $review->delete();

    return response()->json(null, 200);
  }

  public function update(Request $request, User $user)
  {
    $this->validate($request, [
        'body' => 'required|max:2000'
    ]);

    $review = $user->reviews->where('user_id', $request->user()->id)->first();

    $review->body = nl2br(e($request->body));

    $review->save();

    return response()->json(null, 200);
  }
}
