<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PlayerFavoriteController extends Controller
{
  public function __construct()
    {
      $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
        $favorites = $request->user()->favoritePlayers()->with(['playLocations' => function ($query) {
          $query->where('available', 1);
        }])->paginate(8);

        return view('playersfavorites.index', compact('favorites'));
    }

    public function store(Request $request, User $user)
    {
        $request->user()->favoritePlayers()->syncWithoutDetaching([$user->id]);

        return back()->withSuccess('player added to favorites.');
    }

    public function destroy(Request $request, User $user)
    {
        $request->user()->favoritePLayers()->detach($user);

        return back()->withSuccess('player removed from favorites.');
    }
}
