<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'gender' => 'required|in:male,female',
            'skill_level' => 'required|in:2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,7.0',
            'birthdate' => 'required|date',
            'location' => 'required',
            'longitude' => 'required',
            'city' => 'required',
            'right_left' => 'required|numeric'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        // $testdate = date("Y-m-d", strtotime($date['birthdate']));
        // dd($testdate);

        $user = User::create([
            'uid' => uniqid(true),
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'gender' => $data['gender'],
            'skill_level' => $data['skill_level'],
            'birthdate' => date('Y-m-d', strtotime($data['birthdate'])),
            'active' => false,
            'right_left' => $data['right_left'],
            'intro' => 'Hi. Let\'s play tennis!'
        ]);

        $user->last_email_notification = \Carbon\Carbon::now()->subYear(3);
        $user->save();

        $user->playLocations()->create([
          'latitude' => $data['location'],
          'longitude' => $data['longitude'],
          'city' => $data['city']
        ]);

        return $user;
    }

    protected function registered(Request $request, $user)
    {
        Auth::logout();

        return redirect('/')->withInfo('Please activate your account from the email you received.');
    }
}
