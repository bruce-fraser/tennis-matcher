<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class InboxController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
      return view('inbox.index');
    }

    public function show(Message $message)
    {
        $this->authorize('show', $message);

        return view('inbox.index', compact('message'));
    }
}
