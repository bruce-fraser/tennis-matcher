<?php

namespace App\Http\Controllers;

use App\PlayLocation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;

class SearchController extends Controller
{
    public function index(Request $request)
    {
            $this->validate($request, [
               'location' => 'required',
               'longitude' => 'required'
            ]);

        $searchCity = $request['city'];
        $lat = $request['location']; $lng = $request['longitude'];
        $gender = $request['gender']; $skill = $request['skill_level'];
        $radius = intval($request['distance']); $lastOnline = $request['last_online'];
        $rightLeft = $request['right_left']; $singlesDoubles = $request['singles_doubles'];
        $needsPartner = $request['needs_partner'];

        $gender2 = 'not';
        if ($gender == 'any') {
            $gender = 'male'; $gender2 = 'female';
        }
        if ($skill == 'any') {
            $skill = '%';
        }

        $sd2 = '5';
        if ($singlesDoubles == '2') {
          $singlesDoubles = '0';
          $sd2 = '1';
        }

        $lastOnlineStamp = Carbon::now()->subYear(50)->toDateTimeString();
        if ($lastOnline != 'any') {
            $days = intval($lastOnline);
            $lastOnlineStamp = Carbon::now()->subDay($days)->toDateTimeString();
        }

        $genderExclusion = 'none';
        if (Auth::check()) {
          if (Auth::user()->gender == 'male') {
              $genderExclusion = '2';
          } else {
              $genderExclusion = '1';
          }
        }

        $haversine = "(6371 * acos(cos(radians($lat))
                        * cos(radians(`latitude`))
                        * cos(radians(`longitude`)
                        - radians($lng))
                        + sin(radians($lat))
                        * sin(radians(`latitude`))))";
// dd($lastOnlineStamp);
        if ($request->has('map')) {
          $mapQuery = PlayLocation::notBlocked()
            ->join('users', 'play_locations.user_id', '=', 'users.id')
            ->select('users.id as user_id', 'city', 'uid', 'name', 'latitude', 'longitude', 'gender', 'skill_level', 'birthdate', 'active', 'available', 'users.updated_at as user_updated', 'right_left')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ? && `gender` IN (?, ?) && `skill_level` LIKE ? && `active` = 1 && `available` = 1 && users.`updated_at` > ? && `right_left` LIKE ? && `singles_doubles` IN (?, ?, ?) && `need_partner` LIKE ? && `play_with` <> ?", [$radius, $gender, $gender2, $skill, $lastOnlineStamp, $rightLeft, '2', $singlesDoubles, $sd2, $needsPartner, $genderExclusion])
            ->get();
            $mapQuery->map(function($item) {
              $item['avatar_path'] = User::findOrFail($item['user_id'])->avatarPath();
              return $item;
            });
            // dd($mapQuery);
            $players = $mapQuery->toJson(); // dd($players);
            $linkUrl = $request->except('map');
            return view('search.map', compact(['players', 'linkUrl']));
        }

        $dbQuery = PlayLocation::notBlocked()
          ->join('users', 'play_locations.user_id', '=', 'users.id')
          ->select('users.id as user_id', 'city', 'name', 'email', 'gender', 'skill_level', 'birthdate', 'active', 'available', 'users.updated_at as user_updated', 'right_left')
          ->selectRaw("{$haversine} AS distance")
          ->whereRaw("{$haversine} < ? && `gender` IN (?, ?) && `skill_level` LIKE ? && `active` = 1 && `available` = 1 && users.`updated_at` > ? && `right_left` LIKE ? && `singles_doubles` IN (?, ?, ?) && `need_partner` LIKE ? && `play_with` <> ?", [$radius, $gender, $gender2, $skill, $lastOnlineStamp, $rightLeft, '2', $singlesDoubles, $sd2, $needsPartner, $genderExclusion])
          ->orderBy('distance')
          ->paginate(10);
          // ->first();
        // dd($dbQuery);
        $lastOnlineParam = '';
        switch($request['last_online']) {
          case 'any': $lastOnlineParam = 'any time';   break;
          case '1':   $lastOnlineParam = 'today';      break;
          case '7':   $lastOnlineParam = 'this week'; break;
          case '30':  $lastOnlineParam = 'this month'; break;
          default:    $lastOnlineParam = '';
        }
        $rl = '';
        switch($request['right_left']) {
          case '%': $rl = 'either'; break;
          case '1': $rl = 'right-handed'; break;
          case '2': $rl = 'left-handed'; break;
          default: $rl = '';
        }
        $sd = '';
        switch($request['singles_doubles']) {
          case '2': $sd = 'either'; break;
          case '0': $sd = 'plays singles'; break;
          case '1': $sd = 'plays doubles'; break;
          default: $sd = '';
        }
        $np = '';
        if ($request['singles_doubles'] == '1') {
            switch($request['needs_partner']) {
              case '%': $np = 'either'; break;
              case '1': $np = 'needs dbl par'; break;
              case '2': $np = 'has dbl par'; break;
              default: $np = '';
            }
        }
        return view('search.index', [
            'results' => $dbQuery,
            'searchCity' => $searchCity,
            'skill' => $request['skill_level'],
            'gender' => $request['gender'],
            'distance' => intval($request['distance']) > 999 ? 'world' : $request['distance'].' miles',
            'last_online' => $lastOnlineParam,
            'right_left' => $rl,
            'singles_doubles' => $sd,
            'needs_partner' => $request['singles_doubles'] == '1' ? $np : ''
        ]);
    }

    public function show(Request $request, User $user)
    {
        $url = strtok(\URL::previous(), '?');
        // dd($url);
        if ($url == route('search.index') || $url == route('players.favorites.index')) {
          session()->flash('fromresults', 'yes');
        } else {
          session()->flash('fromresults', 'no');
        }

        $user->load(['playLocations' => function ($query) {
          $query->where('available', 1);
        }]);
        // dd($user);
        $ruser = $request->user();
        $reviews = $user->reviews()->notBlocked($ruser)->with('user')->latest()->paginate(8);

        return view('search.show', compact(['user', 'reviews']));
    }
}
