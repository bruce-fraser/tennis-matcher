<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PlayerBlockController extends Controller
{
    public function __construct()
    {
      $this->middleware(['auth']);
    }

    public function store(Request $request, User $user)
    {
        if (!$request->user()->blockedUsers->contains($user)) {
            $request->user()->blockedUsers()->attach($user);
        }

        return redirect()->route('welcome')->withSuccess('Player blocked.');
    }

    public function destroy(Request $request, User $user)
    {
        $request->user()->blockedUsers()->detach($user);

        return back()->withSuccess('Player unblocked.');
    }
}
