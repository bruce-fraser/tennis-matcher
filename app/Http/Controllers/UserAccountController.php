<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserAccountController extends Controller
{
  public function __construct()
    {
      $this->middleware(['auth']);
    }

    public function show(Request $request)
    {
        $blockedUsers = $request->user()->blockedUsers()->get();
        if ($request->user()->stripe_id) {
            $invoices = $request->user()->invoicesIncludingPending();
        } else {
          $invoices = null;
        }
        return view('user.account.show', compact(['blockedUsers', 'invoices']));
    }

    public function updatePassword(Request $request)
    {
      $this->validate($request, [
          'password' => 'required|min:6'
      ]);

      $request->user()->update([
        'password' => bcrypt($request->password)
      ]);

      return back()->withSuccess('password changed successfully!');

    }
}
