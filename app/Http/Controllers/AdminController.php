<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppSettings;
use Stripe\Plan;
use Stripe\Stripe;

class AdminController extends Controller
{
    public function __construct()
    {
      $this->middleware(['auth', 'admin']);
      Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function index() {
      return view('admin.dashboard');
    }

    public function showSettings() {
      $settings = [
        'free_promo_period' => AppSettings::getFreePromoPeriod(),
        'taking_new_subscriptions' => AppSettings::getTakingNewSubscriptions(),
        'hide_site' => AppSettings::getHideSite(),
        'require_register_search' => AppSettings::getRequireRegisterSearch()
      ];

      $plans = collect([]);
      if (config('services.stripe.key')) {
        $stripePlans = Plan::all();
        $plans = $stripePlans['data'];
      }

      return view('admin.settings', compact(['settings', 'plans']));
    }

    public function updateSettings(Request $request) {

      if ($request->free_promo_period == false && $request->taking_new_subscriptions == false) {
          return back()->withProblem("if 'free promo period' is off, then 'taking new subscriptions' must be on");
      }
      if ($request->taking_new_subscriptions == true && !config('services.stripe.key')) {
        return back()->withProblem('the Stripe key is not set!');
      }
      AppSettings::setFreePromoPeriod((bool) $request->free_promo_period);
      AppSettings::setTakingNewSubscriptions((bool) $request->taking_new_subscriptions);
      AppSettings::setHideSite((bool) $request->hide_site);
      AppSettings::setRequireRegisterSearch((bool) $request->require_register_search);
      return back()->withSuccess('ok   ');
    }
}
