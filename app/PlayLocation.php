<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;

class PlayLocation extends Model
{
      protected $fillable = [
        'user_id',
        'latitude',
        'longitude',
        'city',
        'available'
      ];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    /*
     *  S C O P E S
     */
     public function scopeNotBlocked($query)
     {
          if (Auth::guest()) {
            return $query;
          }

          $ids  = Auth::user()->blockedUsers->pluck('id')->toArray();
          $ids2 = Auth::user()->blockedByUsers->pluck('id')->toArray();
          $arr = array_merge($ids, $ids2);

         return $query->whereNotIn('user_id', $arr);
     }

    public function scopeIsWithinMaxDistance($query, $lat, $lng, $radius = 3000) {

       $haversine = "(6371 * acos(cos(radians($lat))
                       * cos(radians(`latitude`))
                       * cos(radians(`longitude`)
                       - radians($lng))
                       + sin(radians($lat))
                       * sin(radians(`latitude`))))";
       return $query
          ->select('*') //pick the columns you want here.
          ->selectRaw("{$haversine} AS distance")
          ->whereRaw("{$haversine} < ?", [$radius])
          ->orderBy('distance');
    }
    /*
     *  E N D  S C O P E S
     */
}
