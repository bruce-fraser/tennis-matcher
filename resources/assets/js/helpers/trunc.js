module.exports = function (str, limit, review = false) {
  if (review == true) {
    var str3 = str.substr(0, limit - 1)
    var newLimit = limit
    if ((str3.match(/<br \/>/g) || []).length > 1) {
      newLimit = limit / 4
    }

    return (str.length > newLimit) ? str.substr(0, newLimit - 1) + '...' : str

  } else {
    var str2 = str.replace(/<br \/>/g, " ")
    return (str2.length > limit) ? str2.substr(0, limit - 1) + '...' : str2
  }
}
