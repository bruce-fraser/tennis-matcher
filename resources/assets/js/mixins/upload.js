export default {
    props: {
        endpoint: {
            type: String
        },
        sendAs: {
            type: String,
            default: 'file'
        }
    },
    data () {
        return {
            uploading: false
        }
    },
    methods: {
        upload (e) {
            this.uploading = true
            console.log(axios.defaults.headers.common)
            return axios.post(this.endpoint, this.packageUploads(e)).then((response) => {
                this.uploading = false
                console.log('check 2: ')
                return Promise.resolve(response)
            }).catch((error) => {
                this.uploading = false
                console.log('check 3: ')
                return Promise.reject(error)
            })
        },
        packageUploads (e) {
            let fileData = new FormData()
            fileData.append(this.sendAs, e.target.files[0])
            console.log('check 4: ' + this.sendAs + ' files: ' + e.target.files[0])
            return fileData
        }
    }
}
