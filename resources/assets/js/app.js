
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('bgf-location', require('./components/BGFLocation.vue'));
Vue.component('avatar-upload', require('./components/AvatarUpload.vue'));
Vue.component('profile-page', require('./components/ProfilePage.vue'));
Vue.component('bgf-inbox', require('./components/BGFInbox.vue'));
Vue.component('bgf-conversation', require('./components/BGFConversation.vue'));
Vue.component('bgf-unread-badge', require('./components/BGFUnreadBadge.vue'));
Vue.component('bgf-unread-fixed', require('./components/BGFUnreadFixed.vue'));
Vue.component('bgf-votes', require('./components/BGFVotes.vue'));
Vue.component('bgf-review', require('./components/BGFReview.vue'));

import store from './store/index'

const app = new Vue({
    el: '#app',
    store
});
