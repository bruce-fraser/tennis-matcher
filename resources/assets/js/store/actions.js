import axios from 'axios'

export const getInboxAction = ({ commit }) => {
  commit('setInboxLoading', true)
  axios.get('/webapi/inbox').then((response) => {
    commit('updateInboxMutation', response.data)
    commit('setInboxLoading', false)

    Echo.private('user.' + Laravel.user.id)
      .listen('ConversationCreated', (e) => {
        commit('prependToConversations', e)
        commit('setSound1')
      })
      .listen('ConversationReplyCreated', (e) => {
        e.parent.body = e.body
        commit('prependToConversations', e.parent)
        commit('setSound1')
      })
  }).catch((error) => {
    console.log('get inbox error ' + error)
  })
}

export const getConversation = ({ dispatch, commit, state }, id) => {
    commit('setConversationLoading', true)

    if (state.conversation) {
      Echo.leave('conversation.' + state.conversation.id);
    }
    axios.get('/webapi/inbox/' + id).then((response) => {
      commit('setConversation', response.data)
      commit('setConversationLoading', false)
      dispatch('updatePivotUnread', id) //new code !!!!!!!!!!!!!!!!!

      Echo.private('conversation.' + id)
        .listen('ConversationReplyCreated', (e) => {
          commit('appendToConversation', e)
          commit('setSound1')
          dispatch('updatePivotUnread', id) //new code !!!!!!!!!!!!!!!
        })

      window.history.pushState(null, null, '/inbox/' + id)
    })
}

export const createConversationReply = ({dispatch, commit}, {id, body}) => {
    axios.post('/webapi/inbox/' + id + '/reply', {
      body: body
    }).then((response) => {
      commit('setSound2')
      commit('appendToConversation', response.data)
      commit('prependToConversations', response.data.parent)
    }).catch((error) => {
      console.log('post reply error: ' + error)
    })
}

export const updatePivotUnread = ({ commit }, id) => {
    console.log('updatePivot id = ' + id)
    axios.get('/webapi/inbox/' + id + '/read').then((response) => {
      console.log(response.data)
    }).catch((error) => {
      console.log('update unread error: ' + error)
    })
}

export const inboxVisibleFlag = ({ commit }) => {
  commit('setInboxVisibleFlag', true)
}

export const loadAudio = ({ commit }) => {
  commit('setAudio')
}
