export const updateInboxMutation = (state, inboxData) => {
  state.inboxState = inboxData;
  updateUnread(state)
  console.log('state.inboxState = ' + state.inboxState)
}

export const setInboxLoading = (state, status) => {
  state.loadingInbox = status
}

export const setConversation = (state, conversation) => {
  state.conversation = conversation

  state.inboxState.forEach((rec) => {
    if (rec.id == conversation.id) { rec.pivot.unread = false }
  })
  updateUnread(state)
}

export const setConversationLoading = (state, status) => {
  state.loadingConversation = status
}

export const appendToConversation = (state, reply) => {
  state.conversation.replies.unshift(reply)
}

export const prependToConversations = (state, conversation) => {
  state.inboxState = state.inboxState.filter((c) => {
    return c.id !== conversation.id
  })
  var unread = { unread: true }
  if (state.conversation && (conversation.id == state.conversation.id)) {
    unread = { unread: false }
  }
  conversation.pivot = unread
  state.inboxState.unshift(conversation)
  updateUnread(state)
}

  function updateUnread (state) {
    var unreadMessages = state.inboxState.filter((c) => {
      return c.pivot.unread == true
    });
    state.unread = unreadMessages.length;
  }

export const setInboxVisibleFlag = (state, status) => {
  state.inboxVisibleFlag = status
}

export const setSound1 = (state) => {
  state.sound1.play()
}

export const setSound2 = (state) => {
  state.sound2.play()
}

export const setAudio = (state) => {
  state.sound1 = new Audio('../sounds/tennisserve.mp3'),
  state.sound2 = new Audio('../sounds/powerfulserve.mp3')
  console.log('enable pushed')
}
