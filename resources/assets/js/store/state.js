export default {
  whatIsThis: 'this is the test string',
  numbers: 3,
  inboxState: null,
  loadingInbox: false,
  loadingConversation: false,
  conversation: null,
  unread: null,
  inboxVisibleFlag: false,
  sound1: new Audio('../sounds/tennisserve.mp3'),
  sound2: new Audio('../sounds/powerfulserve.mp3')
}
