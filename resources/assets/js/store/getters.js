export const numbers = (state) => state.numbers

export const sum = (state) => {
  return state.numbers.reduce((a, b) => a + b, 0)
}

export const inboxGetter = (state) => {
  return state.inboxState
}

export const currentConversation = (state) => {
  return state.conversation
}

export const loadingInbox = (state) => {
  return state.loadingInbox
}

export const inboxVisibleFlag = (state) => {
  return state.inboxVisibleFlag
}

export const loadingConversation = (state) => {
  return state.loadingConversation
}

export const unreadCount = (state) => {
  return state.unread
}

export const sound1 = (state) => {
  return state.sound1
}
