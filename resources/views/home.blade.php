@extends('layouts.app')
@section('title', 'Profile')
@section('content')

    <profile-page uid="{{ Auth::user()->uid }}" endpoint2="{{ route('avatar.store') }}" send-as="image" current-avatar="{{ Auth::user()->avatarPath() }}" user-email="{{ md5(Auth::user()->email) }}"></profile-page>

@endsection

@section('cdn_url')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.maps.key') }}"></script>
@endsection
