@extends('layouts.app')
@section('title', 'Terms of Service')
@section('content')
<div class="container">
    <h2 class="text-center">Bruce Fraser (Tennis Matcher) Terms of Service</h2>

    <p>By using the website you, you agree to be bound by these Terms.  If you do not agree to these Terms, please do not use the website.</p>

    <p>You affirm that you are either at least 18 years of age, or an emancipated minor, or possess legal parental or guardian consent, and are fully able and competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth in these Terms, and to abide by and comply with these Terms. In addition, you affirm that you have not been previously suspended or removed from the website and do not have more than one tennis matcher account.</p>

    <p>We (Bruce Fraser and his agents) may, in our sole discretion, refuse to offer the services of the website to any person or entity. We may, without notice and in our sole discretion, terminate your right to use the website, or any portion thereof, and block or prevent your future access to and use of the website or any portion thereof.</p>
    <p>Privacy</p>
    <p>The tennis matcher Privacy Policy is incorporated into these Terms. By accepting these Terms, you agree to the collection, use, and sharing of your information through the website in accordance with the Privacy Policy, which is available at <a href="{{ route('privacy') }}">{{ route('privacy') }}</a>.</p>

    <p>Fees</p>
    <p>Registration on the site and limited user thereof is free, however, certain features of the site require a paid subscription.  The subscription amount is the posted amount at the time of initiating the subscription via initial payment.  The subscription amount will not change for the duration of that subscription.  However, the subscription price for future subscriptions, not yet initiated, is subject to change.  All fees are non-refundable, even if you cancel before the expiration of the subscription period for which you paid.  If you do cancel your subscription before the expiration of the period for which you paid, your subscription will remain active until the expiration of the paid period.</p>

    <p>User content</p>
    <p>You are responsible for all data charges you may incur by using the website.  You consent to allow other users to view your content with the understanding that you may block specific users who have contacted you on the site from contacting you again.</p>

    <p>You agree that you are solely responsible for your user content and any claims arising therefrom, and the the website (tennis matcher via its administrators) is not responsible or liable for any user content or claims arising therefrom.  The administrators of tennis matcher reserve the right at their discretion to review, screen, and delete user content at any time for any reason.</p>

    <p>You agree to provide true information about yourself as promted by the website.  The administrators of tennis matcher have the right to suspend or terminate your account and refuse future use of the website at their discretion.</p>

    <p>You agree to allow tennis matcher to use any and all content submitted by you in functioning of the website, without tennis matcher or its agents being liable to pay royalties or licensing fees of any sort.</p>

    <p>You acknowledge and agree that tennis matcher may preserve User Content and Membership Data and may also disclose User Content and Membership Data if required to do so by law or in the good faith belief that such preservation or disclosure is reasonably necessary to: (a) comply with legal process; (b) enforce these Terms; (c) respond to claims that any User Content or Membership Data violates the rights of third-parties; or (d) protect the rights, property, or personal safety of tennis matcher, its users, and the public.</p>

    <p>Agreements by you</p>
    <p>You agree to not:</p>
    <ul>
      <li>Use the website for any illegal purpose.</li>
      <li>Impair the website or the ability of other users to enjoy the website</li>
      <li>Send any unauthrorized solicitations to other users.</li>
      <li>Use any automated means to access the website or extract data.</li>
      <li>Use someone else's account without their permission.</li>
      <li>Represent yourself to users on the site as though you are an agent of tennis matcher.</li>
      <li>Engage in harrassment of others.</li>
    </ul>

    <p>We reserve the right to modify or discontinue the services of the website.</p>

    <p>Copyright Infringement</p>
    <p>Please send notices of copyright infringment to <a href="mailto:{{ config('mail.admin') }}?Subject=Copyright%20issue%20-%20tennismatcher" target="_top">{{ config('mail.admin') }}</a> or the business address:</p>
    <p class="mailing-address">
      Bruce Fraser
      2028 E BEN WHITE BLVD., #240-5044
      AUSTIN, TX 78741
    </p>
    <p>Disclaimer</p>
    <p>The services of 'tennis matcher' and its content are provied on an "as is" basis and "as available".  Tennis Matcher makes no reprentations or warranties of any kind, as to the operation of the website, tennis matcher.  You agree that your use of the website is at your sole risk.</p>

    <p>Notice to International Users</p>
    <p>The website is hosted in the United States.  By using the website, you consent to the transfer of your personal information to the United States and the jurisdiction thereof.</p>
    <p>tennis matcher reserves the right to limit the amount of storage and messages and other such digital media which will be available for use by you the user.</p>
    <p>A lawsuit, if any, by your or Bruce Fraser against the other will occur in state or federal court in Dallas County, Texas.  We agree that the jurisdiction and venue of these courts is exclusive.</p>
    <p>Severability</p>
    <p>If any provision of these Terms shall be deemed unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from these Terms and shall not affect the validity and enforceability of any remaining provisions.</p>
    <p>I (Bruce Fraser) may change or modify these Terms at any time.  Changes to the terms will be posted here.  Your continued use of the website will confirm your acceptance of the revised terms.  If you disagree with the terms, you should discontinue use of the website.</p>

    <p>Dated: August 11, 2017</p>
</div>
@endsection
