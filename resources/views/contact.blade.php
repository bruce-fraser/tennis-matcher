@extends('layouts.app')
@section('title', 'Contact Us')
@section('content')
<div class="container">
    <h2 class="text-center">Contact Us</h4>
    <p>Business address:</p>
    <p class="mailing-address">
        Tennis Matcher
        c/o Bruce Fraser
        2028 E BEN WHITE BLVD, #240-5044
        AUSTIN, TX 78741
    </p>
    <p>Or, please send email to: <a href="mailto:{{ config('mail.admin') }}?Subject=Copyright%20issue%20-%20tennismatcher" target="_top">{{ config('mail.admin') }}</a></p>
</div>
@endsection
