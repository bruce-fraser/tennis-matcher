@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">register</div>
                <div class="panel-body">
                    <form class="form-horizontal" id="registrationForm" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="well">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">e-mail address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        </div> <!-- well -->

                        <div class="well">

                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            <label for="gender" class="col-md-4 control-label">gender</label>

                            <div class="col-md-4">
                                <select name="gender" id="gender" class="form-control" required>
                                    <option value=null{{ old('gender') ? '' : ' selected="selected"' }}>< please select ></option>
                                    <option value="male"{{ old('gender') == 'male' ? ' selected="selected"' : '' }}>male</option>
                                    <option value="female"{{ old('gender') == 'female' ? ' selected="selected"' : '' }}>female</option>
                                </select>

                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @include ('layouts.partials._skill_selector')

                        <div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
                            <label for="birthdate" class="col-md-4 control-label">birth date</label>

                            <div class="col-md-4">
                                <input id="birthdate" type="text" class="form-control" name="birthdate" placeholder="MM/DD/YYYY" value="{{ old('birthdate') }}" required>

                                 @if ($errors->has('birthdate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('birthdate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('right_left') ? ' has-error' : '' }}">
                            <label for="right_left" class="col-md-4 control-label">right/left handed</label>

                            <div class="col-md-4">
                                <select name="right_left" id="right_left" class="form-control" required>
                                    <option value=null{{ old('right_left') ? '' : ' selected="selected"' }}>< please select ></option>
                                    <option value=1{{ old('right_left') == 1 ? ' selected="selected"' : '' }}>right handed</option>
                                    <option value=2{{ old('right_left') == 2 ? ' selected="selected"' : '' }}>left handed</option>
                                </select>

                                @if ($errors->has('right_left'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('right_left') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        </div> <!-- well -->

                        <div class="well">

                        <div class="form-group{{ ($errors->has('location') || $errors->has('longitude') || $errors->has('city')) ? ' has-error' : '' }}">
                            <label for="placeInput" class="col-md-4 control-label">ZIP or city, state</label>

                            <div class="col-md-6">
                                <input id="placeInput" type="text" class="form-control" name="placeInput" placeholder="30303 or Atlanta, GA" value="{{ old('placeInput' )}}">

                                @if ($errors->has('location') || $errors->has('longitude') || $errors->has('city'))
                                    <span class="help-block">
                                        <strong>Error processing location, please try again.</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <input id="location" type="text" class="hidden" name="location" value="{{ old('location') }}">
                        <input id="longitude" type="text" class="hidden" name="longitude" value="{{ old('longitude') }}">
                        <input id="city" type="text" class="hidden" name="city" value="{{ old('city') }}">

                        </div> <!-- well -->

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="button" class="btn btn-primary" id="registerButton">
                                    register
                                </button>
                                <p class="text-muted" style="margin-top: 20px;">by registering, you agree to be bound by the 'terms of service' linked to below.</p>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-4 text-center">
                            <hr>
                            <a href="/login" class="btn btn-link">already registered?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('services.maps.key') }}&callback=initMapApi"></script>
<script>

    function initMapApi() {
      var geocoder = new google.maps.Geocoder()
      document.getElementById('registerButton').addEventListener('click', function() {
        var cityInput = document.getElementById('placeInput').value
        if (cityInput == '') {
          alert('please enter a city or zip code.')
          return
        }
        geocoder.geocode({'address': cityInput}, function(results, status) {
          if (status === 'OK') {
             $("#city").val(results[0].formatted_address);
             $("#location").val(results[0].geometry.location.lat());
             $("#longitude").val(results[0].geometry.location.lng());
             document.getElementById('registrationForm').submit()
          } else {
            alert('please enter a valid city or zip code.')
          }
        })
      })
    }

</script>
@endsection
