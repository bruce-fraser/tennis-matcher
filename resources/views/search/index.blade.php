@extends('layouts.app')
@section('title', 'Tennis Partner Results')
@section('content')
<section class="section-searchresults">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                    <div class="heading-search text-center bgf-t-s">
                        <h3 class="heading-searchresults">{{ $results->total() }} results from {{ $searchCity }}</h3>
                        <p><span class="subscriber-icon">S</span> = subscriber</p>
                        <table>
                            <caption class="table-title">
                                PARAMETERS
                            </caption>
                            <tbody>
                                <tr>
                                    <td class="text-right">skill level</td>
                                    <td class="text-left">= {{ $skill }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right">gender</td>
                                    <td class="text-left">= {{ $gender }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right">distance</td>
                                    <td class="text-left">= {{ $distance }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right">last online</td>
                                    <td class="text-left">= {{ $last_online }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right">right/left</td>
                                    <td class="text-left">= {{ $right_left }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right">singles/doubles</td>
                                    <td class="text-left">= {{ $singles_doubles }}</td>
                                </tr>
                                @if ($needs_partner)
                                    <tr>
                                        <td class="text-right">doubles partner</td>
                                        <td class="text-left">= {{ $needs_partner }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                        @if ($results->count())
                            <div class="text-center results-buttons">
                                <a href="{{ route('search.index', array_merge(Request::all(), ['map' => 'yes'])) }}" class="btn btn-primary"><i class="fa fa-map" aria-hidden="true"></i> map</a>
                                <a href="{{ route('welcome') }}" class="btn btn-primary"><i class="fa fa-search"></i> search</a>
                            </div>
                            @foreach ($results as $result)
                                <?php $user = App\User::find($result->user_id) ?>
                                    <a href="{{ route('search.show', $user) }}" class="player-list">
                                        <div class="media media-search">
                                            <div class="media-left media-left-search">
                                                  @if ($user->isSubscriber())
                                                      <span class="subscriber-icon s-icon-index">S</span>
                                                  @endif
                                                  <img src="{{ $user->avatarPath() }}" alt="{{ $result->name }} image" class="img-circle media-object media-object-search">
                                            </div>
                                            <div class="media-body">
                                                <h3 class="text-center">{{ $result->name }} <small>{{ $result->city }}</small></h3>
                                                <p class="text-center">Distance: {{ round($result->distance * 0.62) }} miles</p>

                                                <ul class="list-inline text-center player-info">
                                                    <li>Age: <span class="badge">{{ Carbon\Carbon::parse($result->birthdate)->diffInYears() }}</span></li>
                                                    <li>Level: <span class="badge">{{ $result->skill_level }}</span></li>
                                                    <li>Gender: <span class="badge">{{ $result->gender }}</span></li>
                                                    <li>Last online: <span class="badge">{{ $user->updated_at->diffForHumans() }}</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </a>
                                <!-- </div> -->
                            @endforeach
                            <div class="text-center">
                                {{ $results->appends($_GET)->links() }}
                            </div>
                            @if (auth()->guest())
                                <div class="subscribed-div-results">
                                    <h4 class="text-center">new site! please register so people who come after you can find you and message you!</h4>
                                    <div class="text-center">
                                        <a href="/register" class="btn btn-primary">register</a>
                                        <a href="/login" class="btn btn-primary">login</a>
                                    </div>
                                </div>
                            @endif
                        @else
                            <div class="text-center results-buttons">
                                <a href="{{ route('welcome') }}" class="btn btn-warning"><i class="fa fa-search"></i> search</a>
                            </div>
                            <h4 class="text-center">no players found</h4>
                            @if (auth()->guest())
                                <div class="subscribed-div-results">
                                    <h3 class="text-center">new site! please register so people who come after you can find you and message you!</h3>
                                    <div class="text-center">
                                        <a href="/register" class="btn btn-primary">register</a>
                                        <a href="/login" class="btn btn-primary">login</a>
                                    </div>
                                </div>
                            @endif
                        @endif
            </div>
        </div>
    </div>
</section>
@endsection
