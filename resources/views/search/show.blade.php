@extends('layouts.app')
@section('title', 'Tennis Partner '.$user->name)
@section('content')
<div>
    <section class="profile-header profile-2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center view-profile">
                        @if ($user->isSubscriber())
                            <span class="subscriber-icon s-icon-show">S</span>
                        @endif
                        <img class="img-circle avatar-comp-img" src="{{ $user->avatarPath() }}" alt="Current avatar"> &nbsp; <span class="user-name">{{ $user->name }}</span> &nbsp; <span class="badge">{{ $user->skill_level }}</span>
                    </div>
                    <div class="text-center">
                        <p class="text-muted last-online">last online: <span>{{ $user->updated_at->diffForHumans() }}</span></p>
                    </div>
                </div>
            </div><!-- row -->
        </div><!-- container -->
        @if(Auth::check() && Auth::user()->id == $user->id)
            <a href="{{ route('home') }}" class="btn btn-default btn-sm">edit</a>
        @endif
        @if(Auth::check() && $user->favoritedBy(Auth::user()))
            <i class="fa fa-star corner-star"></i>
        @endif
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                        @if (Auth::guest() || (Auth::user()->id != $user->id))
                        <div class="well text-center profile-button-well">
                              @if (session('fromresults') == 'yes')
                                  <a href="{{ URL::previous() }}" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i> back</a>
                              @endif
                              @if (Auth::guest())
                                  <button type="button" class="btn btn-default" onclick="event.preventDefault(); alert('you must login or register to send message');"><i class="fa fa-envelope" aria-hidden="true"></i>  message</button>
                              @endif
                              @if(Auth::check() && !(Auth::user()->id == $user->id))
                                  @if (App\AppSettings::getFreePromoPeriod() || auth()->user()->isSubscriber() || $user->isSubscriber())
                                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#messageModal"><i class="fa fa-envelope" aria-hidden="true"></i>  message</button>
                                  @else
                                      <button type="button" class="btn btn-default" onclick="event.preventDefault(); alert('you must be a subscriber to message a non-subscriber. you can message a subscriber for free!');"><i class="fa fa-envelope" aria-hidden="true"></i>  message</button>
                                  @endif
                                  <button type="button" class="btn btn-default" onclick="event.preventDefault(); document.getElementById('player-favorite-form').submit();"><i class="fa {{ $user->favoritedBy(Auth::user()) ? 'fa-star-o' : 'fa-star' }}" aria-hidden="true"></i> {{ $user->favoritedBy(Auth::user()) ? 'unfavorite' : 'favorite' }}</button>
                                  <!-- <a href="#" class="btn btn-default"><i class="fa fa-pencil" aria-hidden="true"></i> review</a> -->
                                  <!-- <button type="button" class="btn btn-default"><i class="fa fa-flag" aria-hidden="true"></i> report</button> -->
                                  <button type="button" class="btn btn-default" onclick="if (confirm('You can unblock this user in your account page. Block user?')) { document.getElementById('block-form').submit();}"><i class="fa fa-ban" aria-hidden="true"></i> block</button>
                                  <form action="{{ $user->favoritedBy(Auth::user()) ? route('players.favorites.destroy', $user) : route('players.favorites.store', $user) }}" method="post" id="player-favorite-form" class="hidden">
                                      {{ csrf_field() }}
                                      {{ $user->favoritedBy(Auth::user()) ? method_field('DELETE') : '' }}
                                  </form>
                                  <form class="hidden" id="block-form" action="{{ route('players.block.store', $user) }}" method="post">
                                      {{ csrf_field() }}
                                  </form>
                              @endif
                        </div>
                        @endif
                    <h3 class="text-center">info</h3>
                    <ul class="list-group">
                      <li class="list-group-item">
                        <span class="badge">{{ Carbon\Carbon::parse($user->birthdate)->diffInYears() }}</span>
                        age
                      </li>
                      <li class="list-group-item">
                        <span class="badge">{{ $user->gender }}</span>
                        gender
                      </li>
                      <li class="list-group-item">
                        <span class="badge">{{ $user->skill_level }}</span>
                        level
                      </li>
                      <li class="list-group-item">
                        <span class="badge">{{ $user->right_left == 1 ? 'right-handed': 'left-handed' }}</span>
                        plays
                      </li>
                      <li class="list-group-item">
                        <span class="badge">{{ $user->created_at->diffForHumans() }}</span>
                        member joined
                      </li>
                    </ul>
                    @if ($user->playLocations->count())
                        <h3 class="text-center">available</h3>
                    <div class="list-group">
                    @foreach ($user->playLocations as $location)
                              <div class="list-group-item">
                                  <h4 class="list-group-item-heading">{{ $location->city }}</h4>
                                  <p class="list-group-item-text text-muted list-group-profile-show">
                                      <?php
                                          if ($location->play_with == 1) {
                                            echo 'with males only &bull; ' ;
                                          } elseif ($location->play_with == 2) {
                                            echo 'with females only &bull; ';
                                          }  else {
                                            echo 'with male or female &bull; ';
                                          }
                                          if ($location->singles_doubles == 0) {
                                            echo 'singles';
                                          } elseif ($location->singles_doubles == 1) {
                                            echo 'doubles &bull; ';
                                          } else { echo 'singles &bull; doubles &bull; '; }
                                          if ($location->singles_doubles != 0) {
                                            if ($location->need_partner ==1) {
                                              echo 'need doubles partner';
                                            } else {
                                              echo 'have doubles partner';
                                            }
                                          }
                                       ?>
                                  </p>
                            </div>
                        @endforeach
                        </div>
                        @else
                            <h3 class="text-center">not available to play</h3>
                        @endif
                  </div>
                  <div class="col-md-6">
                        @if ($user->intro != "")
                            <h3 class="text-center">intro</h3>
                            <div class="well">
                              {!! nl2br(e($user->intro)) !!}
                            </div>
                        @endif
                        <div class="text-center">
                          <h3>sportsmanship score</h3>
                          <bgf-votes url-string="{{ route('players.votes.show', $user) }}"></bgf-votes>
                        </div>
                  </div>
            </div> <!-- row -->
            <hr>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="text-center">reviews <span class="badge">{{ $reviews->total() }}</span></h3>

                @if(App\AppSettings::getFreePromoPeriod() || (auth()->check() ? auth()->user()->isSubscriber() : false))

                @if (Auth::check() && !$user->reviewedByUser(Auth::user()) && (Auth::user()->id != $user->id))
                    <div class="row">
                        <div class="col-md-2 text-center">
                            <img src="{{ Auth::user()->avatarPath() }}" alt="{{ Auth::user()->name }} avatar" class="img-circle media-object-3 media-object-reviews">
                            <p class="text-muted">{{ Auth::user()->name }}</p>
                        </div>
                        <div class="col-md-10">
                            <form action="{{ route('players.reviews.store', $user) }}" method="post">
                                <div class="form-group{{ $errors->has('review_field') ? ' has-error' : '' }}">
                                    <textarea class="form-control" placeholder="leave a review ..." rows="3" name="review_field">{{ old('review_field') }}</textarea>
                                    @if ($errors->has('review_field'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('review_field') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default pull-right">send</button>
                                </div>
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div><!-- row -->
                    <hr>
                @endif
                @if ($reviews->count())
                    @foreach ($reviews as $review)
                        <bgf-review avatar="{{ $review->user->avatarPath() }}" name="{{ $review->user->name }}" body="{{ $review->body }}" uid="{{ $review->user->id }}" player-link="{{ route('search.show', $review->user) }}" update-delete-link="{{ route('players.reviews.destroy', $user) }}"></bgf-review>
                    @endforeach
                    <div class="text-center">
                        {{ $reviews->links() }}
                    </div>
                @else
                    <h4 class="text-center">no reviews</h4>
                @endif

                @else
                <div class="review-subscriber-search text-center">
                  <p>must be a suscriber to read and write reviews</p>
                  <p>subscribe for full access! &nbsp; just ${{ App\Appsettings::getSubscriptionPrice() }} / month!</p>
                  <a href="{{ route('welcome') }}#subscribe-div-id" class="btn btn-default">details</a>
                </div>
                @endif
                </div>
            </div><!-- row -->
        </div><!-- container -->
    </section>


              <div class="modal fade" id="messageModal" role="dialog">
                  <div class="modal-dialog">
                      <div class="modal-content">
                        <form action="{{ route('inbox.store') }}" method="POST">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">send message to {{ $user->name }}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <textarea cols="30" rows="4" class="form-control" name="body">I found your profile.  Want to play tennis?</textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                              <button type="submit" class="btn btn-primary">send</button>
                              <button type="button" class="btn btn-default" data-dismiss="modal" id="cancelButton">cancel</button>
                              <input type="hidden" name="recipient" value="{{ $user->id }}">
                            </div>
                            {{ csrf_field() }}
                        </form>
                      </div>
                  </div>
              </div>
</div>


@endsection

@section('custom_scripts')
@endsection
