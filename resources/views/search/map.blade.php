@extends('layouts.map')
@section('title', 'Tennis Partner Map')
@section('content')
    <div class="map-outer-div">
            <div class="map-div" id="map-div">
            </div>
            <span class="button-span-map">
                <a href="{{ route('search.index', $linkUrl) }}" class="btn btn-primary"><i class="fa fa-list"></i> list</a>
                <a href="{{ route('welcome') }}" class="btn btn-primary"><i class="fa fa-search"></i> search</a>
            </span>
    </div>
@endsection

@section('custom_scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('services.maps.key') }}&callback=initMapApi"></script>

<script>
  Laravel.players = {!! isset($players) ? $players : '' !!}

  function initMapApi() {
    var players = Laravel.players
    console.log(players)
    var map = new google.maps.Map(document.getElementById('map-div'), {
      zoom: 2,
      center: new google.maps.LatLng(-37.92, 151.25),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false,
      panControl: false,
      zoomControlOptions: {
         position: google.maps.ControlPosition.LEFT_BOTTOM
      }
    });

    var infowindow = new google.maps.InfoWindow({
      maxWidth: 160
    })

    var markers = new Array()
    // var minLong = 200; var maxLong = -200

    for (var i = 0; i < players.length; i++) {
      // minLong = Math.min(minLong, Number(players[i].longitude)); maxLong = Math.max(maxLong, Number(players[i].longitude));
      var latRand = Math.random() * 0.02 - 0.01; var lngRand = Math.random() * 0.02 - 0.01;
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(Number(players[i].latitude) + latRand, Number(players[i].longitude) + lngRand),
        map: map
      })
      markers.push(marker)
      console.log(players[i].name)
      google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {
          infowindow.setContent('<div style="white-space: nowrap; overflow: hidden;"><a href="/search/' + players[i].uid + '" style="text-decoration: none;"><img src="' + players[i].avatar_path + '" style="width: 35px; border-radius: 50%; margin-right: 8px;"><span><b>' + players[i].name + '</b></span></a></div><p class="text-muted text-right">' + players[i].skill_level + ' ' + players[i].gender + '</p>')
          infowindow.open(map, marker)
        }
      })(marker, i))
    }
      function autoCenter() {
        var bounds = new google.maps.LatLngBounds()
        for (var i = 0; i < markers.length; i++) {
          bounds.extend(markers[i].position)
        }
        map.fitBounds(bounds)
      }
      // if (maxLong - minLong > 50) {
      //   map.center = new google.maps.LatLng(1, 1)
      //   map.zoom = 2
      // } else {
      autoCenter()
      // }
  }
</script>
@endsection
