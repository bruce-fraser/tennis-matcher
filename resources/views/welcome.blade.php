@extends('layouts.app')
@section('title', 'Tennis Partner Search')
@section('content')
<section class="section-1-welcome">
<div class="color-screen"></div>
<div class="container">
    <div class="loader"></div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="heading-container-welcome">
                <h2 class="text-center heading-welcome">Find your match!</h2>
                <span>tennis</span>
                <i class="fa fa-arrow-down"></i>
                <h1 class="text-center">Your website to find a tennis partner!  Search for a singles player or doubles players.  Search according to skill level, gender, and more!</h1>
            </div>
            <div class="panel panel-default panel-welcome">   <!-- panel panel-default -->

                <div class="panel-body">
                    <form action="{{ route('search.index') }}" method="get" id="searchForm">
                    <!-- <form action="{{ route('search.index') }}" method="post"> -->
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group{{ ($errors->has('location') || $errors->has('longitude')) ? ' has-error' : '' }}">
                                <label for="placeInput" class="control-label">search for tennis players by city</label>
                                <input type="text" class="form-control" id="placeInput" name="placeInput"  placeholder="enter city or zip code" autocomplete="off" spellcheck="false">

                                @if ($errors->has('location') || $errors->has('longitude'))
                                    <div class="help-block">
                                        Error processing location, please try again.
                                    </div>
                                @endif
                            </div>
                          </div>
                          <div class="col-md-3">
                            @include ('layouts.partials._skill_selector2')
                          </div>

                          <div class="col-md-3">
                              <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                  <label for="gender" class="control-label">gender</label>

                                      <select name="gender" id="gender" class="form-control">
                                          <option value="any"{{ old('gender') == 'any' ? ' selected="selected"' : '' }}>any gender</option>
                                          <option value="male"{{ old('gender') == 'male' ? ' selected="selected"' : '' }}>male</option>
                                          <option value="female"{{ old('gender') == 'female' ? ' selected="selected"' : '' }}>female</option>
                                      </select>

                                      @if ($errors->has('gender'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('gender') }}</strong>
                                          </span>
                                      @endif
                              </div>
                          </div>

                        </div><!-- row -->
                        <div id="more-options-div">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">distance</label>

                                        <select name="distance" class="form-control" id="distanceSelect">
                                            <option value=20{{ old('distance') == 20 ? ' selected="selected"' : '' }}>20 miles</option>
                                            <option value=2{{ old('distance') == 2 ? ' selected="selected"' : '' }}>2 miles</option>
                                            <option value=10{{ old('distance') == 10 ? ' selected="selected"' : '' }}>10 miles</option>
                                            <option value=50{{ old('distance') == 50 ? ' selected="selected"' : '' }}>50 miles</option>
                                            <option value=99999{{ old('distance') == 99999 ? ' selected="selected"' : '' }}>world</option>
                                        </select>
                                </div>
                            </div>

      @if (App\AppSettings::getFreePromoPeriod() || (auth()->check() ? auth()->user()->isSubscriber() : false))

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">last online</label>

                                        <select name="last_online" class="form-control">
                                            <option value="any"{{ old('last_online') == 'any' ? ' selected="selected"' : '' }}>any time</option>
                                            <option value="1"{{ old('last_online') == '1' ? ' selected="selected"' : '' }}>today</option>
                                            <option value="7"{{ old('last_online') == '7' ? ' selected="selected"' : '' }}>this week</option>
                                            <option value="30"{{ old('last_online') == '30' ? ' selected="selected"' : '' }}>this month</option>
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">right/left handed</label>

                                        <select name="right_left" class="form-control">
                                            <option value="%"{{ old('right_left') == '%' ? ' selected="selected"' : '' }}>either</option>
                                            <option value="1"{{ old('right_left') == '1' ? ' selected="selected"' : '' }}>right handed</option>
                                            <option value="2"{{ old('right_left') == '2' ? ' selected="selected"' : '' }}>left handed</option>
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">singles/doubles</label>

                                        <select name="singles_doubles" class="form-control" id="singles-doubles-select">
                                            <option value="2"{{ old('singles_doubles') == '2' ? ' selected="selected"' : '' }}>either</option>
                                            <option value="0"{{ old('singles_doubles') == '0' ? ' selected="selected"' : '' }}>person plays singles</option>
                                            <option value="1"{{ old('singles_doubles') == '1' ? ' selected="selected"' : '' }}>person plays doubles</option>
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="doubles-has-partner-div">
                            <div class="col-md-3 col-md-offset-9">
                                <div class="form-group">
                                    <label class="control-label">has/needs partner</label>

                                        <select name="needs_partner" class="form-control" id="needs-partner-select">
                                            <option value="%"{{ old('needs_partner') == '%' ? ' selected="selected"' : '' }}>either</option>
                                            <option value="1"{{ old('needs_partner') == '1' ? ' selected="selected"' : '' }}>person needs doubles partner</option>
                                            <option value="2"{{ old('needs_partner') == '2' ? ' selected="selected"' : '' }}>person has doubles partner</option>
                                        </select>
                                </div>
                            </div>
                        </div>
          @else
                    <!-- <span class="subscribers-only-search" -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">last online</label>

                                    <select name="last_online" class="form-control">
                                        <option value="any"{{ old('last_online') == 'any' ? ' selected="selected"' : '' }}>any time</option>
                                        <option value="1"{{ old('last_online') == '1' ? ' selected="selected"' : '' }}>today</option>
                                        <option value="7"{{ old('last_online') == '7' ? ' selected="selected"' : '' }}>this week</option>
                                        <option value="30"{{ old('last_online') == '30' ? ' selected="selected"' : '' }}>this month</option>
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">right/left handed</label>

                                    <select name="right_left" class="form-control">
                                        <option value="%"{{ old('right_left') == '%' ? ' selected="selected"' : '' }}>either</option>
                                        <option value="1"{{ old('right_left') == '1' ? ' selected="selected"' : '' }}>right handed</option>
                                        <option value="2"{{ old('right_left') == '2' ? ' selected="selected"' : '' }}>left handed</option>
                                    </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">singles/doubles</label>

                                    <select name="singles_doubles" class="form-control">
                                        <option value="2"{{ old('singles_doubles') == '2' ? ' selected="selected"' : '' }}>either</option>
                                        <option value="0"{{ old('singles_doubles') == '0' ? ' selected="selected"' : '' }}>person plays singles</option>
                                        <option value="1"{{ old('singles_doubles') == '1' ? ' selected="selected"' : '' }}>person plays doubles</option>
                                    </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-md-offset-9">
                            <div class="form-group">
                                <label class="control-label">has/needs partner</label>

                                    <select name="needs_partner" class="form-control">
                                        <option value="%"{{ old('needs_partner') == '%' ? ' selected="selected"' : '' }}>either</option>
                                        <option value="1"{{ old('needs_partner') == '1' ? ' selected="selected"' : '' }}>person needs doubles partner</option>
                                        <option value="2"{{ old('needs_partner') == '2' ? ' selected="selected"' : '' }}>person has doubles partner</option>
                                    </select>
                            </div>
                        </div>
                    </div>
                    <div class="subscribers-only-search">
                      <span>subscribers only</span>
                    </div>
                    <!-- </span> -->
          @endif
                        </div>
                        @if (!App\AppSettings::getRequireRegisterSearch() || auth()->check())
                            <button class="btn btn-trans search-button-welcome" id="searchButton" type="button">search</button>
                        @else
                            <a href="/register" onclick="alert('please register or login to search.')" class="btn btn-trans search-button-welcome">search</a>
                        @endif
                        <input id="location" type="text" class="hidden" name="location" value="{{ old('location') }}">
                        <input id="longitude" type="text" class="hidden" name="longitude" value="{{ old('longitude') }}">
                        <input id="city" type="text" class="hidden" name="city" value="{{ old('city') }}">
                        <input id="more-less" type="text" class="hidden" name="more_less" value="{{ old('more_less') }}">

                        {{ csrf_field() }}

                        <button type="button" id="options-button" class="btn btn-trans search-button-welcome pull-right">more options &nbsp;<i class="fa fa-arrow-down"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <span class="photo-credit-welcome">photo by Georgio</span>
</div>
</section>
<section class="section-2-welcome">
  <div class="container">
      <div class="row intro-text-welcome">
          <div class="col-md-6">
              <h3 class="text-center">New site!</h3>
              <p class="text-left">Hello, fellow tennis players.  I have been having trouble finding a tennis partner so I made a website. Please register so that tennis players who register after you can find you. There will be few players on here in the beginning, but hopefully more people will sign up.</p>
              <div class="text-center">
                  <img src="{{ asset('img/tennis-partners.jpg') }}" alt="old fashioned tennis partners" class="tennis-partners-welcome">
              </div>
          </div>
          <div class="col-md-6">
              <p class="text-left">I plan on making it always free to message or receive a messge from a subscriber, so there should be people to contact even if you don't want to subscribe. Subscribers can message and receive messages from non-subscribers and subscribers alike.</p>
              <h4 class="text-center">Contact</h4>
              <p class="text-left">
                If you want to give me feedback, you can message me at my profile <a href="{{ route('search.show', $user) }}">Bruce Fraser</a> or via <a href="mailto:{{ config('mail.admin') }}?Subject=From%20welcome%20page%20-%20tennismatcher" target="_top">email</a>.
              </p>
              <p>Thank you for checking out the site!</p>
              <p class="text-right">- Bruce Fraser</p>
          </div>
      </div>
  </div>
</section>
<section class="section-3-welcome">
  <div class="container">
      <div class="row">
          <div class="col-md-6 col-md-offset-3">
              @if (auth()->check() && auth()->user()->isSubscriber())
                  <div class="subscribed-div-welcome">
                      <h4 class="text-center">Thanks for subscribing!</h4>
                      <ul class="list-group">
                        <li class="list-group-item text-center"><b>Manage your:</b></li>
                        <li class="list-group-item text-center"><a href="{{route('home')}}" class="btn btn-trans">profile</a><a href="{{route('user.account.show')}}" class="btn btn-trans">account</a></li>
                        <li class="list-group-item text-center"><a href="{{route('inbox.index')}}" class="btn btn-trans">messages</a><a href="{{route('players.favorites.index')}}" class="btn btn-trans">favorites</a></li>
                      </ul>
                  </div>
              @else
                  @if (App\AppSettings::getTakingNewSubscriptions())
                      <div id="subscribe-div-id"></div>
                      <div class="subscribe-div-welcome">
                          <h4 class="text-center">just ${{ App\AppSettings::getSubscriptionPrice() }} per month!</h4>
                          <ul class="list-group">
                              <li class="list-group-item">Send and receive messages from both subscribers and non-subscribers.</li>
                              <li class="list-group-item">Read and write reviews on other players.</li>
                              <li class="list-group-item">Search based on: recently online, left-handed/right-handed, singles/doubles, has doubles partner/needs doubles partner, and more.</li>
                          </ul>
                          @if (auth()->check())
                              <form action="{{ route('subscription.store') }}" method="post" id="payment-form">
                                  {{ csrf_field() }}
                                  <input id="s-token" type="hidden" name="stripeToken" value="">
                              </form>
                              <div class="button-div-welcome text-center">
                                  <button id="customButton" class="btn btn-primary">subscribe</button>
                              </div>
                              <br>
                              <p class="text-muted text-center">Cancel at any time</p>
                              <p class="text-muted text-center">By subscribing, you agree to the Terms of Service</p>
                          @else
                              <p class="text-center"><a href="/login" class="btn btn-trans">login</a> &nbsp; or &nbsp; <a href="/register" class="btn btn-trans">register</a> &nbsp; in order to subscribe.</p>
                          @endif
                      </div>
                  @else
                      <div class="subscribed-div-welcome">
                          @if (auth()->check())
                              <h4 class="text-center">Thank you for registering at Tennis Matcher!</h4>
                              <ul class="list-group">
                                <li class="list-group-item text-center"><b>Manage your:</b></li>
                                <li class="list-group-item text-center"><a href="{{route('home')}}" class="btn btn-trans">profile</a><a href="{{route('user.account.show')}}" class="btn btn-trans">account</a></li>
                                <li class="list-group-item text-center"><a href="{{route('inbox.index')}}" class="btn btn-trans">messages</a><a href="{{route('players.favorites.index')}}" class="btn btn-trans">favorites</a></li>
                              </ul>
                          @else
                              <h4 class="text-center">Register and let people find you.  Send messages.</h4>
                              <p class="text-center"><a href="/register" class="btn btn-trans">register</a> or <a href="/login" class="btn btn-trans">login</a></p>
                          @endif
                      </div>
                  @endif
              @endif
              @if (auth()->check() && !auth()->user()->isSubscriber() && App\AppSettings::getTakingNewSubscriptions())
              <div class="subscribed-div-welcome">
                  <h4 class="text-center">Thank you for registering at Tennis Matcher!</h4>
                  <ul class="list-group">
                    <li class="list-group-item text-center"><b>Manage your:</b></li>
                    <li class="list-group-item text-center"><a href="{{route('home')}}" class="btn btn-trans">profile</a><a href="{{route('user.account.show')}}" class="btn btn-trans">account</a></li>
                    <li class="list-group-item text-center"><a href="{{route('inbox.index')}}" class="btn btn-trans">messages</a><a href="{{route('players.favorites.index')}}" class="btn btn-trans">favorites</a></li>
                  </ul>
              </div>
              @endif
          </div>
      </div>
      <div class="video-welcome">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/NPHnhZ8LY1I" allowfullscreen></iframe>
      </div>
      <div class="row search-cities-welcome">
          <!-- <div class="container"> -->
              <div class="panel panel-default">
                  <div class="panel-heading">
                      search cities
                  </div>
                  <div class="panel-body">
                      <div class="col-sm-2 col-xs-6">
                          <a href="#" onclick="event.preventDefault(); cityLink('atlanta')">Atlanta</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('austin, tx')">Austin</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('baltimore')">Baltimore</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Boston')">Boston</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Charlotte')">Charlotte</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Chicago')">Chicago</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Cincinnati')">Cincinnati</a>
                      </div>
                      <div class="col-sm-2 col-xs-6">
                          <a href="#" onclick="event.preventDefault(); cityLink('cleveland')">Cleveland</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('colorado springs')">Colorado Springs</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('columbus, oh')">Columbus, OH</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Dallas')">Dallas</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('denver')">Denver</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Detroit')">Detroit</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('fort worth')">Fort Worth</a>
                      </div>
                      <div class="col-sm-2 col-xs-6">
                          <a href="#" onclick="event.preventDefault(); cityLink('Houston')">Houston</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('indianapolis')">Indianapolis</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('jacksonville, fl')">Jacksonville, FL</a>
                          <!-- <a href="#" onclick="event.preventDefault(); cityLink('kansas city')">Kansas City</a> -->
                          <a href="#" onclick="event.preventDefault(); cityLink('las vegas')">Las Vegas</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('los angeles')">Los Angeles</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('miami')">Miami</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Milwaukee')">Milwaukee</a>
                      </div>
                      <div class="col-sm-2 col-xs-6">
                        <a href="#" onclick="event.preventDefault(); cityLink('Minneapolis')">Minneapolis</a>
                        <a href="#" onclick="event.preventDefault(); cityLink('Nashville')">Nashville</a>
                        <a href="#" onclick="event.preventDefault(); cityLink('New Orleans')">New Orleans</a>
                        <a href="#" onclick="event.preventDefault(); cityLink('nyc')">New York City</a>
                        <a href="#" onclick="event.preventDefault(); cityLink('orlando')">Orlando</a>
                        <a href="#" onclick="event.preventDefault(); cityLink('philadelphia')">Philadelphia</a>
                        <a href="#" onclick="event.preventDefault(); cityLink('Phoenix')">Phoenix</a>
                      </div>
                      <div class="col-sm-2 col-xs-6">
                          <a href="#" onclick="event.preventDefault(); cityLink('pittsburgh')">Pittsburgh</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('portland')">Portland</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Providence')">Providence</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Sacramento')">Sacramento</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Saint Louis')">Saint Louis</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('San Antonio')">San Antonio</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('San Diego')">San Diego</a>
                      </div>
                      <div class="col-sm-2 col-xs-6">
                          <a href="#" onclick="event.preventDefault(); cityLink('san francisco')">San Francisco</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('San Jose')">San Jose</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Seattle')">Seattle</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Tampa')">Tampa</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('Tucson')">Tucson</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('washington dc')">Washington DC</a>
                          <a href="#" onclick="event.preventDefault(); cityLink('salt lake city')">Salt Lake City</a>
                      </div>
                  </div>
              </div>
        </div>
    </div>
</section>
@endsection

@section('custom_scripts')
@if (auth()->check() && !auth()->user()->isSubscriber() && App\AppSettings::getTakingNewSubscriptions())
<script src="https://checkout.stripe.com/checkout.js"></script>
@endif
@if (!App\AppSettings::getRequireRegisterSearch() || auth()->check())
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('services.maps.key') }}&callback=initMapApi"></script>
@endif
<script type="text/javascript">

@if (!App\AppSettings::getRequireRegisterSearch() || auth()->check())
  function initMapApi() {
    var geocoder = new google.maps.Geocoder()
    document.getElementById('searchButton').addEventListener('click', function() {
      var cityInput = document.getElementById('placeInput').value
      if (cityInput == '') {
        alert('please enter a city or zip code.')
        return
      }
      document.querySelector(".loader").style.display = "block";
      geocoder.geocode({'address': cityInput}, function(results, status) {
        if (status === 'OK') {
           $("#city").val(results[0].formatted_address);
           $("#location").val(results[0].geometry.location.lat());
           $("#longitude").val(results[0].geometry.location.lng());
           document.getElementById('searchForm').submit()
           document.querySelector(".loader").style.display = "none";
        } else {
          document.querySelector(".loader").style.display = "none";
          alert('please enter a valid city or zip code.')
        }
      })
    })
  }
@endif

  function cityLink(city) {
    @if (!App\AppSettings::getRequireRegisterSearch() || auth()->check())
    document.getElementById('placeInput').value = city
    document.getElementById('distanceSelect').value = 50
    document.getElementById('searchButton').click()
    @else
      alert('please register or login to search')
      window.location.href="{{ config('app.url') }}/register"
    @endif
  }

   @if (App\AppSettings::getFreePromoPeriod() || (auth()->check() ? auth()->user()->isSubscriber() : false))

   $("#options-button").click(function () {
       $("#more-options-div").toggle(500, function () {
           if ($("#more-options-div").css('display') == 'none') {
             $("#options-button").html("more options &nbsp;<i class='fa fa-arrow-down'></i>");
             $("#more-less").val('closed');
           } else {
             $("#options-button").html("less options &nbsp;<i class='fa fa-arrow-up'></i>");
             $("#more-less").val('open');
             if ($("#singles-doubles-select").val() != '1') {
                 $("#needs-partner-select").val('%');
                 $("#doubles-has-partner-div").hide();
             }
           }
       });
   });

   $("#singles-doubles-select").change(function () {
     if ($("#singles-doubles-select").val() == '1') {
       $("#doubles-has-partner-div").show(500);
     } else {
       $("#needs-partner-select").val('%');
       $("#doubles-has-partner-div").hide(500);
     }
   });
   $("#doubles-has-partner-div").hide();
   if ($("#more-less").val() == 'open') {
       $("#options-button").click();
       if ($("#singles-doubles-select").val() == '1') {
           $("#doubles-has-partner-div").show();
       }
   }

   @else

      $("#options-button").click(function () {
           $("#more-options-div").toggle(500, function () {
               if ($("#more-options-div").css('display') == 'none') {
                 $("#options-button").html("more options &nbsp;<i class='fa fa-arrow-down'></i>");
                 $("#more-less").val('closed');
               } else {
                 $("#options-button").html("less options &nbsp;<i class='fa fa-arrow-up'></i>");
                 $("#more-less").val('open');
               }
           });
       });
        if ($("#more-less").val() == 'open') {
            $("#options-button").click();
        }

   @endif

@if (auth()->check() && !auth()->user()->isSubscriber() && App\AppSettings::getTakingNewSubscriptions())

var handler = StripeCheckout.configure({
  key: '{{ config('services.stripe.key') }}',
  image: '{{ asset('img/tmlogoshadow.png') }}',
  locale: 'auto',
  zipCode: true,
  token: function(token) {
    document.getElementById('s-token').setAttribute('value', token.id);
    document.getElementById('payment-form').submit();
    // You can access the token ID with `token.id`.
  }
});
document.getElementById('customButton').addEventListener('click', function(e) {
  // Open Checkout with further options:
  handler.open({
    name: 'Bruce Fraser (Tennis M)',
    description: 'monthly subscription',
    amount: {{ App\AppSettings::getSubscriptionPrice() * 100 }}
  });
  e.preventDefault();
});
// Close Checkout on page navigation:
window.addEventListener('popstate', function() {
  handler.close();
});
@endif

</script>
@endsection
@section('schema_org')
<script type="application/ld+json">
{
    "@context" : "http://schema.org",
    "@type" : "Organization",
    "name" : "Tennis Matcher",
    "url" : "{{ config('app.url') }}/",
    "logo": "{{ asset('img/tmfav-256.png') }}",
    "sameAs" : [
        "https://www.facebook.com/tennismatcher.net/",
        "https://twitter.com/tennis_matcher",
    ]
}
</script>
@endsection
