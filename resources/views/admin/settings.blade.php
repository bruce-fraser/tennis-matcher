@extends('layouts.admin')

@section('content')
        <h3 class="text-center">settings</h3>
        <hr>
        <div class="bgf-card">
            <h3 class="text-center">switches</h3>
            <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.updateSettings') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="free_promo_period" {{ $settings['free_promo_period'] == true ? 'checked' : '' }}> free promo period
                                  </label>
                              </div>
                        </div>
                        <div class="form-group">
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="taking_new_subscriptions" {{ $settings['taking_new_subscriptions'] == true ? 'checked' : '' }}> taking new subscriptions
                                  </label>
                              </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="hide_site" {{ $settings['hide_site'] == true ? 'checked' : '' }}> hide site (include 'noindex')
                                  </label>
                              </div>
                        </div>
                        <div class="form-group">
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="require_register_search" {{ $settings['require_register_search'] == true ? 'checked' : '' }}> require registration to search
                                  </label>
                              </div>
                        </div>
                    </div>
                </div>
                @if (session()->has('problem'))
                    <br>
                    <span class="alert alert-danger">
                        <strong>{!! session()->get('problem') !!}</strong>
                    </span>
                @endif
                <div class="form-group">
                    <div class="col-md-1 col-md-offset-11">
                        <button type="submit" class="btn btn-primary">
                            save
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="bgf-card">
            <h3 class="text-center">stripe plans</h3>

            <table class="table">
               <thead>
                 <tr>
                   <th>plan name</th>
                   <th>plan id</th>
                   <th>amount</th>
                   <th>interval</th>
                 </tr>
               </thead>
               <tbody>
                 @foreach ($plans as $plan)
                 <tr>
                   <td>{{ $plan->name }}</td>
                   <td>{{ $plan->id }}</td>
                   <td>${{ number_format($plan->amount/100, 2) }}</td>
                   <td>{{ $plan->interval }}</td>
                 </tr>
                 @endforeach
               </tbody>
             </table>
        </div>
@endsection
