@extends('layouts.app')
@section('title', 'messages')
@section('content')
<section class="messages-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <!-- <div class="panel panel-success">
                    <div class="panel-heading">
                        inbox
                    </div>
                    <div class="panel-body inbox-panel"> -->
                        <bgf-inbox></bgf-inbox>
                    <!-- </div>
                </div> -->
            </div>
            <div class="col-md-7">
                @if (isset($message))
                    <bgf-conversation id="{{ $message->id }}" route-inbox="{{ route('inbox.index') }}" auth-user-subscribed="{{ auth()->user()->isSubscriber() }}" free-promo-mode="{{ App\AppSettings::getFreePromoPeriod() }}"></bgf-conversation>
                @else
                    <bgf-conversation route-inbox="{{ route('inbox.index') }}" auth-user-subscribed="{{ auth()->user()->isSubscriber() }}" free-promo-mode="{{ App\AppSettings::getFreePromoPeriod() }}"></bgf-conversation>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
