@component('mail::message')
# Someone wants to play.

You have unread messages in your inbox.

@component('mail::button', ['url' => route('inbox.index') ])
messages
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
