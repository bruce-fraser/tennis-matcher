@component('mail::message')
# Tennis Matcher Account Activation

Please click the button to activate your account.

@component('mail::button', ['url' => $url])
Activate Account
@endcomponent

@component('mail::panel')
Or, copy and past the following:

<span style="font-size: 8px;">{{ $url }}</span>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
