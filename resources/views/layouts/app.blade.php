<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta_tag')
    <meta name="description" content="Tennis Matcher is a website for tennis players to help them find someone to play (tennis partner) - either singles or doubles.">
    <meta name="keywords" content="tennis, player, partner, tennis partner, singles, doubles, tennis match, tennis player, tennis doubles, find, finder">
    <meta name="author" content="Bruce Fraser">
    @if (App\AppSettings::getHideSite())
      <meta name="robots" content="noindex">
    @endif
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/tmlogo-57.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/tmlogo-72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/tmlogo-114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120 152x152 180x180" href="{{ asset('img/tmfav-256.png') }}"/>
    <link rel="shortcut icon" type="image/png" sizes="16x16 32x32" href="{{ asset('img/tmfav.png') }}"/>
    <link rel="shortcut icon" type="image/png" sizes="128x128 256x256" href="{{ asset('img/tmfav-256.png') }}"/>

    <meta name="og:image" content="{{ asset('img/tmfav-256.png') }}">
    <meta name="og:image:width" content="256">
    <meta name="og:image:height" content="256">
    <meta name="og:type" content="website">
    <meta name="og:url" content="{{ config('app.url') }}/">
    <meta name="og:title" content="Tennis Matcher | find your (tennis) match">
    <meta name="og:description" content="Search for a tennis partner - singles or doubles!">
    <meta name="og:site_name" content="Tennis Matcher">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => [
                'id' => Auth::check() ? Auth::user()->id : null
            ],
            'keys' => [
              'pusher' => config('broadcasting.connections.pusher.key'),
              'cluster' => config('broadcasting.connections.pusher.options.cluster')
            ]
        ]) !!};
    </script>
    @yield('schema_org')
</head>
<body>
    <div id="app">
        @include ('layouts.partials.nav._main')
        @include ('layouts.partials._subscribe')
        @include ('layouts.partials._notifications')
        @yield('content')
        @include ('layouts.partials._footer')
    </div>

    <!-- Scripts -->
        @yield('cdn_url')
    <script src="{{ asset('js/app.js') }}"></script>
        @yield('custom_scripts')
</body>
</html>
