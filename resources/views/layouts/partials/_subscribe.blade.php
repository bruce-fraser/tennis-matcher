@if (!App\AppSettings::getFreePromoPeriod() && App\AppSettings::getTakingNewSubscriptions() && (auth()->check() ? !auth()->user()->isSubscriber() : true))
<div class="subscribe-banner">
    <span>subscribe now for full access just ${{ number_format(App\AppSettings::getSubscriptionPrice(), 2) }} / month</span> &nbsp; <a href="{{ route('welcome') }}#subscribe-div-id" class="btn-sm btn-default">details</a>
</div>
<div class="banner-spacer">.</div>
@elseif (App\AppSettings::getFreePromoPeriod() && (auth()->check() ? !auth()->user()->isSubscriber() : true))
<div class="promo-banner">free promotional period - unlimited use</div>
<div class="banner-spacer">.</div>
@endif
