<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h4 class="text-center">Tennis Matcher</h4>
                <ul class="list-inline text-center">
                    <li>&copy; 2017 Bruce Fraser</li>
                    <li><a href="{{ route('terms') }}">Terms of Service</a></li>
                    <li><a href="{{ route('privacy') }}">Privacy Policy</a></li>
                    <li><a href="{{ route('contact') }}">Contact Us</a></li>
                    <li><a href="{{ route('links') }}">Tennis Links</a></li>
                </ul>
                <ul class="list-inline text-center social-ul">
                    <li><a href="https://twitter.com/tennis_matcher/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.facebook.com/tennismatcher.net/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <bgf-unread-fixed></bgf-unread-fixed>
</footer>
