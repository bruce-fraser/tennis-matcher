<nav class="admin-nav">
    <aside class="admin-aside">
      <h3 class="text-center">admin</h3>
      <hr>
      <ul>
        <li><a href="{{ route('admin.index') }}">dashboard</a></li>
        <li><a href="{{ route('admin.settings') }}">settings</a></li>
        <li><a href="#">statistics</a></li>
      </ul>

    </aside>
</nav>
