<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('img/tennis-matcher.png') }}" alt="{{ config('app.name')}} brand" class="brand-img">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
              <li><a href="{{ route('welcome') }}"><i class="fa fa-search" aria-hidden="true"></i>find tennis partner</a></li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">login</a></li>
                    <li><a href="{{ route('register') }}">register</a></li>
                @else
                    <li><a href="{{ route('players.favorites.index') }}"><i class="fa fa-star" aria-hidden="true"></i>favorites</a></li>
                    <li><a href="{{ route('inbox.index') }}"><i class="fa fa-envelope" aria-hidden="true"></i>messages <bgf-unread-badge></bgf-unread-badge></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                          <img src="{{ Auth::user()->avatarPath(30) }}" alt="{{ Auth::user()->name }} image" class="img-circle nav-image">
                          <span class="name-nav">{{ Auth::user()->name }} </span><span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('home') }}"><i class="fa fa-user icon-nav" aria-hidden="true"></i>profile</a></li>
                            <li><a href="{{ route('user.account.show') }}"><i class="fa fa-credit-card-alt icon-nav" aria-hidden="true"></i>account</a></li>
                            <li role="separator" class="divider"></li>
                            @if (Auth::check() && auth()->user()->isAdmin())
                            <li><a href="{{ route('admin.index') }}" rel="nofollow"><i class="fa fa-unlock icon-nav" aria-hidden="true"></i>admin panel</a></li>
                            <li role="separator" class="divider"></li>
                            @endif
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out icon-nav" aria-hidden="true"></i>logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
