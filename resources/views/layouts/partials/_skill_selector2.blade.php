<div class="form-group{{ $errors->has('skill_level') ? ' has-error' : '' }}">
    <label for="skill_level" class="control-label">skill level</label>

        <select name="skill_level" id="skill_level" class="form-control">
            <option value="any"{{ old('skill_level') == 'any' ? ' selected="selected"' : '' }}>any level</option>
            <option value="2.0"{{ old('skill_level') == '2.0' ? ' selected="selected"' : '' }}>2.0 - Beginner</option>
            <option value="2.5"{{ old('skill_level') == '2.5' ? ' selected="selected"' : '' }}>2.5 - Advanced Beginner</option>
            <option value="3.0"{{ old('skill_level') == '3.0' ? ' selected="selected"' : '' }}>3.0 - Intermediate</option>
            <option value="3.5"{{ old('skill_level') == '3.5' ? ' selected="selected"' : '' }}>3.5 - Advanced Intermediate</option>
            <option value="4.0"{{ old('skill_level') == '4.0' ? ' selected="selected"' : '' }}>4.0 - Competitor</option>
            <option value="4.5"{{ old('skill_level') == '4.5' ? ' selected="selected"' : '' }}>4.5 - Advanced Competitor</option>
            <option value="5.0"{{ old('skill_level') == '5.0' ? ' selected="selected"' : '' }}>5.0 - Expert</option>
            <option value="5.5"{{ old('skill_level') == '5.5' ? ' selected="selected"' : '' }}>5.5 - Advanced Expert</option>
            <option value="6.0"{{ old('skill_level') == '6.0' ? ' selected="selected"' : '' }}>6.0 - Tournament Player</option>
            <option value="7.0"{{ old('skill_level') == '7.0' ? ' selected="selected"' : '' }}>7.0 - Professional Player</option>
        </select>

        @if ($errors->has('skill_level'))
            <span class="help-block">
                <strong>{{ $errors->first('skill_level') }}</strong>
            </span>
        @endif
</div>
