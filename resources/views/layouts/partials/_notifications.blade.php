<!-- <div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3"> -->
            @if (session()->has('error'))
                <div class="alert alert-danger resend-notification">
                    {!! session()->get('error') !!}
                </div>
            @endif

            @if (session()->has('errors'))
                <div class="alert alert-danger alert-anim">
                    {!! session()->get('errors')->first() !!}
                </div>
            @endif

            @if (session()->has('success'))
                <div class="alert alert-success alert-anim">
                    {!! session()->get('success') !!}
                </div>
            @endif

            @if (session()->has('info'))
                <div class="alert alert-success alert-anim">
                    {!! session()->get('info') !!}
                </div>
                <script type="text/javascript">
                    alert('please check your email for the activation link.');
                </script>
            @endif
        <!-- </div>
    </div>
</div> -->
