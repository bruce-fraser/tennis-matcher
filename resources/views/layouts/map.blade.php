<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
      <meta name="robots" content="noindex">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} @yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/tmlogo-57.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/tmlogo-72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/tmlogo-114.png') }}">
    <link rel="shortcut icon" type="image/png" sizes="16x16 32x32" href="{{ asset('img/tmfav.png') }}"/>
    <link rel="shortcut icon" type="image/png" sizes="128x128 256x256" href="{{ asset('img/tmfav-256.png') }}"/>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => [
                'id' => Auth::check() ? Auth::user()->id : null
            ],
            'keys' => [
              'pusher' => config('broadcasting.connections.pusher.key'),
              'cluster' => config('broadcasting.connections.pusher.options.cluster')
            ]
        ]) !!};
    </script>
</head>
<body>
        @yield('content')

    <!-- Scripts -->
        @yield('custom_scripts')
</body>
</html>
