<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- SEO -->
    <meta name="googlebot" content="noindex">
    <meta name="robots" content="noindex">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - admin</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => [
                'id' => Auth::check() ? Auth::user()->id : null
            ],
            'keys' => [
              'pusher' => config('broadcasting.connections.pusher.key'),
              'cluster' => config('broadcasting.connections.pusher.options.cluster')
            ]
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        @include ('layouts.partials.nav._main')
        @include ('layouts.partials.nav._admin')
        @include ('layouts.partials._notifications')
        <div class="admin-detail-area">
            @yield('content')
        </div>
    </div>

    <!-- Scripts -->
        @yield('cdn_url')
    <script src="{{ asset('js/app.js') }}"></script>
        @yield('custom_scripts')
</body>
</html>
