@extends('layouts.app')
@section('title', 'Tennis Links')
@section('content')
<div class="container">
    <h2 class="text-center">Tennis Links</h4>
    <p>The following are links which are useful to many tennis players:</p>
    <ul>
      <li><a href="https://www.usta.com/" target="_blank">USTA (United States Tennis Association)</a></li>
      <li><a href="http://www.tennis.com/" target="_blank">TENNIS.com</a></li>
      <li><a href="http://www.usopen.org/" target="_blank">usopen.org</a></li>
    </ul>
</div>
@endsection
