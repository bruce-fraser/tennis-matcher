@extends('layouts.app')
@section('title', '| account')
@section('content')
<section class="profile-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <img class="img-circle avatar-comp-img" src="{{ Auth::user()->avatarPath() }}" alt="Current avatar"> &nbsp; <span class="user-name">{{ Auth::user()->name }}</span>
                </div>
            </div>
        </div><!-- row -->
    </div><!-- container -->
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                @if (count($invoices))
                <div class="panel panel-default">
                    <div class="panel-heading">
                        invoices
                    </div>
                    <div class="panel-body">
<!-- I N V O I C E S -->
                        <table class="table">
                            <thead>
                              <tr>
                                <th>invoice date</th>
                                <th>charge</th>
                                <th>&nbsp;</th>
                              </tr>
                            </thead>
                            <tbody>
                            @foreach ($invoices as $invoice)
                              <tr>
                                  <td>{{ $invoice->date()->toFormattedDateString() }}</td>
                                  <td>{{ $invoice->total() }}</td>
                                  <td><a href="/user/invoice/{{ $invoice->id }}">download</a></td>
                              </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">
                        change password
                    </div>
                    <div class="panel-body">
                        <form role="form" action="{{ route('user.account.password') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">new password</label>

                                <div>
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary">submit</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading open-button-div">
                        blocked users
                        <button type="button" class="btn-sm btn-default" onclick="toggleBlocked()">open/close</button>
                    </div>
                    <div class="panel-body panel-body-account" id="blockedUsers">
                        @if ($blockedUsers->count())
                            @foreach ($blockedUsers as $user)
                                <div class="media">
                                      <div class="media-left media-middle">
                                          <img src="{{ $user->avatarPath() }}" alt="{{ $user->name }} image" class="img-circle media-object media-object-account">
                                      </div>
                                      <div class="media-body">
                                          <h4 class="blocked-name-account">{{ $user->name }}</h4>
                                      </div>
                                      <div class="media-right media-middle">
                                          <a href="#" class="btn btn-default" onclick="event.preventDefault(); document.getElementById('unblock-form-{{ $user->id }}').submit();">unblock</a>
                                      </div>
                                </div>
                                <form action="{{ route('players.block.destroy', $user) }}" method="post" id="unblock-form-{{ $user->id }}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                            @endforeach
                        @else
                            <h4 class="text-center">no blocked players</h4>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                @if (!auth()->user()->isAdmin())
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            subscription
                        </div>
                        <div class="panel-body text-center">
                            @if (auth()->user()->subscribed('standard') && !auth()->user()->subscription('standard')->onGracePeriod())
<!-- C A N C E L  S U B S C R I P T I O N -->
                                <p>you are currently subscribed.</p>
                                <button type="button" class="btn btn-primary" onclick="cancelSubscription()">cancel my subscription</button>
                                <form action="{{ route('subscription.delete') }}" method="post" id="cancel-form">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <input type="hidden" name="cancel_subscription" value="true">
                                </form>
                                <hr>
<!-- C H A N G E  C A R D -->
                                <p>your current card on file is:</p>
                                <p><span class="card-span-account"><b>{{ auth()->user()->card_brand }}</b> last four digits: <b>{{ auth()->user()->card_last_four }}</b></span></p>
                                <p>change your credit card.</p>
                                <form action="{{ route('subscription.updateCard') }}" method="post" id="payment-form">
                                    {{ csrf_field() }}
                                    <input id="s-token" type="hidden" name="stripeToken" value="">
                                </form>
                                <button id="customButton" class="btn btn-primary">change card</button>
                            @elseif (auth()->user()->subscription('standard') && auth()->user()->subscription('standard')->onGracePeriod())
<!-- R E S U M E  S U B S C R I P T I O N -->
                                <p>your subscription will end on {{ auth()->user()->subscription('standard')->ends_at->toFormattedDateString() }}</p>
                                <button type="button" class="btn btn-primary" onclick="resumeSubscription()">resume subscription</button>
                                <form action="{{ route('subscription.resume') }}" method="post" id="resume-form">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="resume_subscription" value="true">
                                </form>
                            @else
                                <p>you are not currently subscribed.</p>
                                @if(App\AppSettings::getTakingNewSubscriptions())
                                    <a href="{{ route('welcome')}}#subscribe-div-id" class="btn btn-primary">subscribe</a>
                                @endif
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>

@endsection

@section('custom_scripts')
@if (auth()->check() && !auth()->user()->isAdmin())
<script src="https://checkout.stripe.com/checkout.js"></script>
@endif
<script type="text/javascript">
    function cancelSubscription () {
      if (confirm('are you sure you want to cancel? (click "cancel" to NOT cancel - i know)')) {
        document.getElementById('cancel-form').submit()
      }
    }

    function resumeSubscription () {
        document.getElementById('resume-form').submit()
    }

    @if (auth()->check() && !auth()->user()->isAdmin())
    var handler = StripeCheckout.configure({
      key: '{{ config('services.stripe.key') }}',
      image: '{{ asset('img/tmlogoshadow.png') }}',
      locale: 'auto',
      zipCode: 'true',
      token: function(token) {
        document.getElementById('s-token').setAttribute('value', token.id);
        document.getElementById('payment-form').submit();
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
      }
    });

    document.getElementById('customButton').addEventListener('click', function(e) {
      // Open Checkout with further options:
      handler.open({
        name: 'Bruce Fraser (tennis m-)',
        panelLabel: 'Update Card Details',
        label: 'Update Card Details',
        allowRememberMe: false,
        email: '{{ auth()->user()->email }}'
      });
      e.preventDefault();
    });

    // Close Checkout on page navigation:
    window.addEventListener('popstate', function() {
      handler.close();
    });
    @endif
    function toggleBlocked () {
      $('#blockedUsers').toggle(500);
    }
    $('#blockedUsers').toggle();
</script>
@endsection
