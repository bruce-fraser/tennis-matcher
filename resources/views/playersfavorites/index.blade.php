@extends('layouts.app')
@section('title', 'favorites')
@section('content')
<section class="section-fav">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center favorites-banner">
                  <h1><i class="fa fa-star"></i> favorites <span class="badge badge-white fav-badge">{{ $favorites->total() }}</span></h1>
                </div>
                @if ($favorites->count())
                    @foreach ($favorites as $favorite)
                          <a href="{{ route('search.show', $favorite) }}" class="player-list">
                                <div class="media media-fav">
                                        <div class="media-left media-middle">
                                              <img src="{{ $favorite->avatarPath() }}" alt="{{ $favorite->name }} image" class="img-circle media-object media-object-fav">
                                        </div>
                                    <div class="media-body">
                                            <h3 class="text-center name-fav">{{ $favorite->name }}</h3>
                                        @if ($favorite->playLocations->count())
                                        <ul class="list-inline text-center loc-list-fav">
                                            @foreach ($favorite->playLocations as $location)
                                                <li class="muted city-fav">{{ $location->city }}</li>
                                            @endforeach
                                        </ul>
                                        @else
                                            <p class="text-center">not available to play</p>
                                        @endif
                                        <ul class="list-inline text-center player-info-fav">
                                            <li>Age: <span class="badge">{{ $favorite->birthdate->diffInYears() }}</span></li>
                                            <li>Level: <span class="badge">{{ $favorite->skill_level }}</span></li>
                                            <li>Gender: <span class="badge">{{ $favorite->gender }}</span></li>
                                            <li>Last online: <span class="badge">{{ $favorite->updated_at->diffForHumans() }}</span></li>
                                        </ul>
                                    </div>
                                </div>
                          </a>
                    @endforeach
                    <div class="text-center">
                        {{ $favorites->links() }}
                    </div>
                @else
                <h2 class="text-center no-results">no favorites</h2>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
