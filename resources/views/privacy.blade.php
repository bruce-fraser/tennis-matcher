@extends('layouts.app')
@section('title', 'Privacy Policy')
@section('content')
<div class="container">
    <h4 class="text-center">Bruce Fraser (tennis matcher) Privacy Policy</h4>
    <p>This privacy policy discloses the privacy practices of {{ config('app.name') }}.  </p>
    <p>{{ config('app.name') }}'s owner, Bruce Fraser, is the sole owner of the information collected on this website.  He will not sell or rent this information to anyone.'</p>

    <p>Unless you request otherwise, {{ config('app.name') }}'s owner may contact you via email regarding use of the website or pomotional material related to the website.</p>

    <p>Registration</p>
    <p>In order to use the website, beyond just browsing, registration is required, and disclosure of your name, age, gender, handed-ness(?), and email is required.  Uploading of a picture/avatar is optional.  This information is visible to other users of the site - both registered and not registered.  You may make your account not included in search results by making profile 'unavailable' at all locations on your profile page.</p>

    <p>Credit Card Information</p>
    <p>Should you elect to subscribe to the website via your credit card, only the type of credit card (eg., Visa, Mastercard, etc.) and the last four digits are stored by {{ config('app.name') }}.  Your credit card information is collected by <a href="https://stripe.com">Stripe</a>.</p>
    <p>Please direct any questions or comments to: <a href="mailto:{{ config('mail.admin') }}?Subject=Privacy%20issue%20-%20tennismatcher" target="_top">{{ config('mail.admin') }}</a></p>
    <p>Dated: August 11, 2017</p>
</div>
@endsection
